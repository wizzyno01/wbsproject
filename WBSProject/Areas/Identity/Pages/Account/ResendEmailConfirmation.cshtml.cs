using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WBSProject.Areas.Identity.Pages.Account
{
    public class ResendEmailConfirmationModel : PageModel
    {
        public class InputModel
        {
            [Required, EmailAddress, StringLength(50)]
            public string Email { get; set; }
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl ?? Url.Content("~/");
        }

        public IActionResult OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");

            if (ModelState.IsValid)
            {
                return RedirectToPage("./RegisterConfirmation", new { email = Input.Email, returnUrl });
            }

            return Page();
        }
    }
}

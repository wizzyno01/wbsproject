﻿using AutoMapper;
using FormHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Areas.WBS.Models;
using WBSProject.Data;
using WBSProject.Models.EF;
using WBSProject.Sms;

namespace WBSProject.Areas.WBS.Controllers
{
    [Area(Globals.Areas.WBS)]
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppMessages _messageTemplates;
        private readonly IMapper _mapper;
        private readonly ISmsSender _smsSender;
        public DashboardController(ApplicationDbContext context, AppMessages messageTemplates, IMapper mapper, ISmsSender smsSender)
        {
            _context = context;
            _messageTemplates = messageTemplates;
            _mapper = mapper;
            _smsSender = smsSender;
        }

        //public async Task<IActionResult> SendTestSms()
        //{
        //    await _smsSender.SendSmsAsync("Testing Infobip Sms Service.", "IWORLDHMS", " + 2348069328151");
        //    return RedirectToAction(nameof(Index));
        //}

        public async Task<IActionResult> Index()
        {
            var applicantInfo = await _context.ApplicantInfos
                .Include(i => i.State)
                .Include(i => i.LGA)
                .Include(i => i.Ward)
                .SingleOrDefaultAsync(i => i.Username == User.Identity.Name);

            var applicantId = applicantInfo?.Id;
            var grantInfo = await _context.GrantInfos.SingleOrDefaultAsync(g => g.ApplicantId == applicantId);

            ViewBag.HasGrantInfo = grantInfo?.CanSubmit;
            return View(applicantInfo);
        }

        public async Task<IActionResult> Profile()
        {
            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(i => i.Username == User.Identity.Name);

            if (applicantInfo != null && applicantInfo.Usage != WBSProject.Models.DataUsage.General)
                return RedirectToAction(nameof(Index));

            var model = applicantInfo == null ? null : _mapper.Map<RegisterApplicantPersonal>(applicantInfo);
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Profile(RegisterApplicantPersonal model)
        {
            if (ModelState.IsValid)
            {
                var applicantInfo = _mapper.Map<ApplicantInfo>(model);
                applicantInfo.Username = User.Identity.Name;

                if (model.Id == null)
                    _context.ApplicantInfos.Add(applicantInfo);
                else
                {
                    applicantInfo = await _context.ApplicantInfos.SingleAsync(i => i.Id == model.Id);
                    applicantInfo = _mapper.Map(model, applicantInfo);
                    _context.ApplicantInfos.Update(applicantInfo);
                }
                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Profile registered successfully", nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Business()
        {
            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(i => i.Username == User.Identity.Name);

            if (applicantInfo != null && applicantInfo.Usage != WBSProject.Models.DataUsage.General)
                return RedirectToAction(nameof(Index));

            if (applicantInfo == null)
                return RedirectToAction(nameof(Profile));

            var model = _mapper.Map<RegisterApplicantBusiness>(applicantInfo);
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Business(RegisterApplicantBusiness model)
        {
            if (ModelState.IsValid)
            {
                var applicantInfo = await _context.ApplicantInfos.SingleAsync(i => i.Id == model.Id);
                applicantInfo = _mapper.Map(model, applicantInfo);

                _context.ApplicantInfos.Update(applicantInfo);
                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Business registered successfully", nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Contact()
        {
            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(i => i.Username == User.Identity.Name);

            if (applicantInfo != null && applicantInfo.Usage != WBSProject.Models.DataUsage.General)
                return RedirectToAction(nameof(Index));

            if (applicantInfo == null || string.IsNullOrEmpty(applicantInfo.BusinessName))
                return RedirectToAction(nameof(Business));

            var model = _mapper.Map<RegisterApplicantContact>(applicantInfo);
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Contact(RegisterApplicantContact model)
        {
            if (ModelState.IsValid)
            {
                var applicantInfo = await _context.ApplicantInfos.SingleAsync(i => i.Id == model.Id);
                applicantInfo = _mapper.Map(model, applicantInfo);

                _context.ApplicantInfos.Update(applicantInfo);
                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Business Contact registered successfully", nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Grant()
        {
            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(i => i.Username == User.Identity.Name);

            if (applicantInfo != null && applicantInfo.Usage != WBSProject.Models.DataUsage.General)
                return RedirectToAction(nameof(Index));

            if (applicantInfo == null || applicantInfo.WardId == null)
                return RedirectToAction(nameof(Contact));

            var grantInfo = await _context.GrantInfos.SingleOrDefaultAsync(i => i.ApplicantId == applicantInfo.Id);

            if (grantInfo == null)
            {
                grantInfo = new GrantInfo
                {
                    Id = Guid.NewGuid(),
                    ApplicantId = applicantInfo.Id,
                    FullNames = applicantInfo.FullNames,
                    BusinessName = applicantInfo.BusinessName
                };
                _context.GrantInfos.Add(grantInfo);
                await _context.SaveChangesAsync();
            }

            var model = _mapper.Map<RegisterApplicantGrant>(grantInfo);
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Grant(RegisterApplicantGrant model)
        {
            if (ModelState.IsValid)
            {
                var grantInfo = await _context.GrantInfos.SingleOrDefaultAsync(i => i.ApplicantId == model.ApplicantId);
                grantInfo = _mapper.Map(model, grantInfo);

                _context.GrantInfos.Update(grantInfo);
                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Grant information updated successfully", nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Submit()
        {
            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(i => i.Username == User.Identity.Name);

            if (applicantInfo == null)
                return RedirectToAction(nameof(Index));

            var model = _mapper.Map<RegistrationSummary>(applicantInfo);

            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Submit(Guid id)
        {
            var applicantInfo = await _context.ApplicantInfos.SingleAsync(i => i.Id == id);
            applicantInfo.Usage = WBSProject.Models.DataUsage.Official;

            await _context.SaveChangesAsync();

            var template = _messageTemplates.Find(Globals.MessageCategories.GuestDashboard.SubmitApplication);

            if (!(template == null || string.IsNullOrEmpty(template.Content)))
            {
                var subject = template.Subject ?? "WBS";
                await _smsSender.SendSmsAsync(template.Content, subject, applicantInfo.PhoneNumber);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost, AllowAnonymous]
        public async Task<JsonResult> GetLGAs(Guid state)
        {
            var lgas = await _context.Lgas.Where(l => l.StateId == state).ToArrayAsync();
            return Json(lgas);
        }

        [HttpPost, AllowAnonymous]
        public async Task<JsonResult> GetWards(Guid lga)
        {
            var wards = await _context.Wards.Where(l => l.LgaId == lga).ToArrayAsync();
            return Json(wards);
        }
    }
}

﻿using FormHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Areas.WBS.Models;
using WBSProject.Biometrics;
using WBSProject.Data;
using WBSProject.Helpers;
using WBSProject.Models.EF;
using WBSProject.Components.Paging;
using AutoMapper;
using System.IO;
using OfficeOpenXml;
using WBSProject.Domain.SharedComponent;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WBSProject.Areas.WBS.Controllers
{
    [Area(Globals.Areas.WBS)]
    [Authorize(Policy = "AdminAndAudit")]
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ISharedComponents _sharedComponents;
        private readonly IMapper _mapper;

        public AdminController(ApplicationDbContext context, IMapper mapper, ISharedComponents sharedComponents)
        {
            _context = context;
            _mapper = mapper;
            _sharedComponents = sharedComponents;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Applications(int? page, string search)
        {
            var query = _context.ApplicantInfos
                .Include(a => a.State)
                .Include(a => a.LGA)
                .Include(a => a.Ward)
                .Where(a => a.Usage == WBSProject.Models.DataUsage.Official);

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(a =>
                    a.Surname.Contains(search) || a.FirstName.Contains(search) || a.OtherName.Contains(search) ||
                    a.BusinessName.Contains(search) || a.PhoneNumber.StartsWith(search));
            }

            var applications = await query.ToPagedListAsync(page ?? 1, 10);

            ViewBag.Search = search;
            return View(applications);
        }
        
           public async Task<IActionResult> FilterApplication(int? page, Guid? lgaId, string gender, string search, bool download)
        {
            var query = _context.ApplicantInfos
                .Include(a => a.State)
                .Include(a => a.LGA)
                .Include(a => a.Ward)
                .Where(a => a.Usage == WBSProject.Models.DataUsage.Official);

            var lga = _context.Lgas.Select(x => new LGAData { Id = x.Id, Name = x.Name }).ToList();

            ViewBag.lgas = lga.Prepend(new LGAData { Id = null, Name = "Select L.G.A" });

            ViewBag.genders = new List<Gender>() { new Gender { Value = "", DisplayName = "Select Gender" }, new Gender { Value = "male", DisplayName = "Male" }, new Gender { Value = "female", DisplayName = "Female" } };

            if (!string.IsNullOrEmpty(lgaId.ToString()))
            {
                query = query.Where(x => x.LGAId == lgaId);
                ViewBag.lgaId = lgaId;
            }

            if (!string.IsNullOrEmpty(gender))
            {
                query = query.Where(x => x.Gender == gender);
                ViewBag.gender = gender;
            }

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(a =>
                    a.Surname.Contains(search) || a.FirstName.Contains(search) || a.OtherName.Contains(search) ||
                    a.BusinessName.Contains(search) || a.PhoneNumber.StartsWith(search) || a.Address.Contains(search));

                ViewBag.search = search;
            }

            if (download)
            {
                var stream = new MemoryStream();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    var setdata = query.ToList().Select(x => { x.BusinessStatus = x.BusinessStatus == "Registered" ? $"{x.BusinessStatus} ({x.RCNumber})" : x.BusinessStatus;  x.Age = _sharedComponents.CalculateAge(Convert.ToDateTime(x.BirthDate)); x.StateName = _sharedComponents.StateName(x.StateId); x.LGAName = _sharedComponents.LGaName(x.LGAId); x.WardName = _sharedComponents.WardName(x.WardId); return x; });
                    workSheet.Cells.LoadFromCollection(setdata.ToList().Select(x => new {
                        // Profile
                        x.FullNames,
                        x.Surname,
                        x.FirstName,
                        x.OtherName,
                        x.Email,
                        x.PhoneNumber,
                        x.Gender,
                        x.Age,
                        x.StateName,
                        x.LGAName,
                        x.WardName,
                        x.Address,
                        // Business 
                        x.BusinessName,
                        x.BusinessAddress,
                        x.NatureOfBusiness,
                        x.BusinessStatus,
                        x.AgeOfBusiness,
                        x.NumberOfEmployees
                        
                    }), true);
                    package.Save();
                }
                stream.Position = 0;
                string excelName = $"ApplicationData-{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";
                return File(stream, "application/octet-stream", excelName);
            }


            var applications = await query.ToPagedListAsync(page ?? 1, 10);

            return View(applications);

        }

        
        // Added this, i don't know much about your code so i used your above code to get "ApplicantInfos" 
        public IActionResult DownloadExcel()
        {
            var query = _context.ApplicantInfos
               .Include(a => a.State)
               .Include(a => a.State)
               .Include(a => a.LGA)
               .Include(a => a.Ward);

            var stream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(query, true);
                package.Save();
            }
            stream.Position = 0;
            string excelName = $"CompletedApplication-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
            return File(stream, "application/octet-stream", excelName);
        }


        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> CaptureBiometrics(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(a => a.Id == id);

            if (applicantInfo == null)
                return NotFound();

            var biometricInfo = await _context.BiometricInfos.SingleOrDefaultAsync(b => b.ApplicantId == id);

            var model = new CaptureBiometrics
            {
                ApplicantId = applicantInfo.Id,
                FullNames = applicantInfo.FullNames,
                Session = await User.GetOrAddCaptureSession(_context)
            };

            if (biometricInfo != null)
            {
                model.ApplicantId = applicantInfo.Id;
                model.FullNames = applicantInfo.FullNames;

                model.Photo = biometricInfo.Photo.ToJpegWebImage();
                model.RightThumbImage = biometricInfo.RightThumbImage.ToJpegWebImage();
                model.RightIndexImage = biometricInfo.RightIndexImage.ToJpegWebImage();
                model.LeftThumbImage = biometricInfo.LeftThumbImage.ToJpegWebImage();
                model.LeftIndexImage = biometricInfo.LeftIndexImage.ToJpegWebImage();
            }

            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> CaptureBiometrics(CaptureBiometrics model)
        {
            if (ModelState.IsValid)
            {
                var biometricInfo = await _context.BiometricInfos.SingleOrDefaultAsync(b => b.ApplicantId == model.ApplicantId);

                if (biometricInfo == null)
                {
                    biometricInfo = new BiometricInfo
                    {
                        Id = Guid.NewGuid(),
                        ApplicantId = model.ApplicantId,
                        FullName = model.FullNames,

                        Photo = model.Photo.ToBinaryImage(),
                        RightThumbImage = model.RightThumbImage.ToBinaryImage(),
                        RightIndexImage = model.RightIndexImage.ToBinaryImage(),
                        LeftThumbImage = model.LeftThumbImage.ToBinaryImage(),
                        LeftIndexImage = model.LeftIndexImage.ToBinaryImage(),
                        RightIndexTemplate = model.RightIndexMinutia.ToBinaryImage(),
                        RightThumbTemplate = model.RightThumbMinutia.ToBinaryImage(),
                        LeftIndexTemplate = model.LeftIndexMinutia.ToBinaryImage(),
                        LeftThumbTemplate = model.LeftThumbMinutia.ToBinaryImage()
                    };
                    _context.BiometricInfos.Add(biometricInfo);
                }
                else
                {
                    biometricInfo.FullName = model.FullNames;

                    biometricInfo.Photo = model.Photo.ToBinaryImage();
                    biometricInfo.RightThumbImage = model.RightThumbImage.ToBinaryImage();
                    biometricInfo.RightIndexImage = model.RightIndexImage.ToBinaryImage();
                    biometricInfo.LeftThumbImage = model.LeftThumbImage.ToBinaryImage();
                    biometricInfo.LeftIndexImage = model.LeftIndexImage.ToBinaryImage();
                    biometricInfo.RightIndexTemplate = model.RightIndexMinutia.ToBinaryImage();
                    biometricInfo.RightThumbTemplate = model.RightThumbMinutia.ToBinaryImage();
                    biometricInfo.LeftIndexTemplate = model.LeftIndexMinutia.ToBinaryImage();
                    biometricInfo.LeftThumbTemplate = model.LeftThumbMinutia.ToBinaryImage();
                }

                var session = await _context.CaptureSessions.SingleOrDefaultAsync(s => s.Id == model.Session);
                session.State = SessionState.Close;

                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Biometrics captured successfully.", Url.Action(nameof(Applications)));
            }
            return View(model);
        }

        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> CaptureDocuments(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(a => a.Id == id);

            if (applicantInfo == null)
                return NotFound();

            var documentInfo = await _context.Documents.FirstOrDefaultAsync(b => b.ApplicantId == id);

            var model = new CaptureDocuments
            {
                ApplicantId = applicantInfo.Id,
                FullNames = applicantInfo.FullNames,
                BusinessName = applicantInfo.BusinessName,
                Session = await User.GetOrAddCaptureSession(_context)
            };

            if (documentInfo != null)
            {
                model.Documents = documentInfo.Content.ToWebPdf();
            }

            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> CaptureDocuments(CaptureDocuments model)
        {
            if (ModelState.IsValid)
            {
                var documentInfo = await _context.Documents.FirstOrDefaultAsync(b => b.ApplicantId == model.ApplicantId);

                if (documentInfo == null)
                {
                    var document = new Document
                    {
                        Id = Guid.NewGuid(),
                        Type = DocumentType.Pdf,
                        ApplicantId = model.ApplicantId,
                        BusinessName = model.BusinessName,
                        FullNames = model.FullNames,
                        Content = model.Documents.FromWebDocument()
                    };
                    _context.Documents.Add(document);
                }
                else
                {
                    documentInfo.Content = model.Documents.FromWebDocument();
                }

                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Documents captured successfully.", Url.Action(nameof(Applications)));
            }

            return View(model);
        }

        public async Task<IActionResult> Status(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var applicantInfo = await _context.ApplicantInfos
                .Include(a => a.State)
                .Include(a => a.LGA)
                .Include(a => a.Ward)
                .SingleOrDefaultAsync(a => a.Id == id);

            if (applicantInfo == null)
                return NotFound();

            var biometricInfo = await _context.BiometricInfos.SingleOrDefaultAsync(b => b.ApplicantId == id);
            var grantInfo = await _context.GrantInfos
                .Include(g => g.DLI)
                .SingleOrDefaultAsync(g => g.ApplicantId == id);

            var model = _mapper.Map<ApplicationStatus>(applicantInfo);

            if (biometricInfo != null)
                model = _mapper.Map(biometricInfo, model);

            if (grantInfo != null)
                model = _mapper.Map(grantInfo, model);

            return View(model);
        }

        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> EditProfile(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(i => i.Id == id);

            if (applicantInfo == null)
                return NotFound();

            var model = _mapper.Map<RegisterApplicantPersonal>(applicantInfo);
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProfile(RegisterApplicantPersonal model)
        {
            if (ModelState.IsValid)
            {
                var applicantInfo = await _context.ApplicantInfos.SingleAsync(i => i.Id == model.Id);

                applicantInfo = _mapper.Map(model, applicantInfo);
                _context.ApplicantInfos.Update(applicantInfo);
                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Profile updated successfully", Url.Action(nameof(Status), new { model.Id }));
            }
            return View(model);
        }

        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> EditBusiness(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(i => i.Id == id);

            if (applicantInfo == null)
                return NotFound();

            var model = _mapper.Map<RegisterApplicantBusiness>(applicantInfo);
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBusiness(RegisterApplicantBusiness model)
        {
            if (ModelState.IsValid)
            {
                var applicantInfo = await _context.ApplicantInfos.SingleAsync(i => i.Id == model.Id);

                applicantInfo = _mapper.Map(model, applicantInfo);
                _context.ApplicantInfos.Update(applicantInfo);
                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Business updated successfully", Url.Action(nameof(Status), new { model.Id }));
            }
            return View(model);
        }

        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> EditGrant(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var grantInfo = await _context.GrantInfos.SingleOrDefaultAsync(i => i.Id == id);

            if (grantInfo == null)
                return NotFound();

            var model = _mapper.Map<RegisterApplicantGrant>(grantInfo);
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> EditGrant(RegisterApplicantGrant model)
        {
            if (ModelState.IsValid)
            {
                var grantInfo = await _context.GrantInfos.SingleAsync(i => i.Id == model.Id);

                grantInfo = _mapper.Map(model, grantInfo);
                _context.GrantInfos.Update(grantInfo);
                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Grant information updated successfully",
                    Url.Action(nameof(Status), new { id = model.ApplicantId }));
            }
            return View(model);
        }

        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> DeleteApplication(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var applicantInfo = await _context.ApplicantInfos
                .Include(a => a.State)
                .Include(a => a.LGA)
                .Include(a => a.Ward)
                .SingleOrDefaultAsync(i => i.Id == id);

            if (applicantInfo == null)
                return NotFound();

            return View(applicantInfo);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteApplication(Guid id)
        {
            var applicantInfo = await _context.ApplicantInfos.SingleAsync(i => i.Id == id);
            var biometricInfo = await _context.BiometricInfos.SingleOrDefaultAsync(bi => bi.ApplicantId == id);
            var grantInfo = await _context.GrantInfos.SingleOrDefaultAsync(gi => gi.ApplicantId == id);

            _context.Remove(applicantInfo);

            if (biometricInfo != null)
                _context.Remove(biometricInfo);

            if (grantInfo != null)
                _context.Remove(grantInfo);

            await _context.SaveChangesAsync();

            
            return RedirectToAction(nameof(Applications));
        }
    }
}

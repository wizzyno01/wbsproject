using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Data;
using WBSProject.Domain.SharedComponent;
using WBSProject.Models.EF;

namespace WBSProject.Areas.WBS.Controllers
{
    [Area(Globals.Areas.WBS)]
    [Route("api/[controller]")]
    [ApiController]
    public class GpsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ISharedComponents _sharedComponents;
        private readonly IMapper _mapper;

        public GpsController(ApplicationDbContext context, IMapper mapper, ISharedComponents sharedComponents)
        {
            _context = context;
            _mapper = mapper;
            _sharedComponents = sharedComponents;
        }

        [HttpGet]
        public async Task<IEnumerable<GeoLocation>> GetGeoLocations()
        {
            return await _context.GeoLocations.ToListAsync();
        }

        [HttpGet("{applicantId}")]
        public async Task<ActionResult<GeoLocation>> GetGeoLocation(Guid? applicantId)
        {
            var data = await _context.GeoLocations.SingleAsync(x => x.ApplicantId == applicantId);
            if(data == null)
            {
                return BadRequest();
            }
            return Ok(data);
        }


        [HttpPost]
        public async Task<ActionResult<GeoLocation>> PostGeoLocation([FromBody] GeoLocation model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.GeoLocations.Add(model);
            try
            {
                int result = await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (GeolocationExists(model.ApplicantId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = model.Id }, model);//Ok(result); 
        }

        [HttpPut]
        public async Task<ActionResult> PutGeoLocation([FromBody] GeoLocation model)
        {
            var data = _context.GeoLocations.SingleOrDefault(x => x.ApplicantId == model.ApplicantId);

            if(data == null)
            {
                return BadRequest();
            }

            data = _mapper.Map(model, data);
            _context.GeoLocations.Update(data);
            await _context.SaveChangesAsync();

            return Ok(data);
        }

        [HttpPost("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            GeoLocation gpsData = await _context.GeoLocations.FindAsync(id);
            if (gpsData == null)
            {
                return NotFound();
            }

            _context.GeoLocations.Remove(gpsData);
            await _context.SaveChangesAsync();

            return Ok(gpsData);
        }

        private bool GeolocationExists(Guid id)
        {
            return _context.GeoLocations.Count(e => e.ApplicantId == id) > 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WBSProject.Areas.WBS.Models;
using WBSProject.Components.Paging;
using WBSProject.Models.Identity;

namespace WBSProject.Areas.WBS.Controllers
{
    [Area(Globals.Areas.WBS)]
    [Authorize]
    public class SecurityController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IEmailSender emailSender;
        private readonly ILogger<SecurityController> logger;

        public SecurityController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender, ILogger<SecurityController> logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.emailSender = emailSender;
            this.logger = logger;
        }

        [Authorize(Policy = "SuperUser")]
        public async Task<IActionResult> Index(int? page, string search)
        {
            var query = userManager.Users;

            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(a =>
                    a.FirstName.Contains(search) || a.LastName.Contains(search) ||
                    a.UserName.Contains(search) || a.PhoneNumber.StartsWith(search));
            }

            var users = await query.OrderBy(user => user.UserName).ToPagedListAsync(page ?? 1, 10);

            ViewBag.Search = search;
            return View(users);
        }

        [AllowAnonymous]
        public ActionResult SignUp()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> SignUp(SignUpViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    UserName = model.Email,
                    Type = ApplicationUserType.Guest
                };
                var result = await userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    logger.LogInformation("Signup account created successfully.");

                    result = await userManager.AddToRoleAsync(user, Globals.Roles.Guest);

                    if (result.Succeeded)
                    {
                        logger.LogInformation("Signup account added to guest role.");
                    }

                    var returnUrl = Url.Action("Index", "Dashboard", new { area = Globals.Areas.WBS });
                    //var returnUrl = Url.Action("Dashboard", "Home", new { area = Globals.Areas.WBS });
                    var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));

                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code, returnUrl },
                        protocol: Request.Scheme);
                    //await emailSender.SendEmailAsync(model.Email, "Confirm your email",
                    //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    await Task.Run(() =>
                    {
                        emailSender.SendEmailAsync(model.Email, "Confirm your email",
                            $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
                    });


                    if (userManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToAction(nameof(SignUpConfirmation));
                    }
                    else
                    {
                        await signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }

                }
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult SignUpConfirmation()
        {
            return View();
        }

        [Authorize(Policy = "SuperUser")]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(SignUpViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Type = ApplicationUserType.Audit,
                    EmailConfirmed = true
                };
                var result = await userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            return View(model);
        }
    }
}
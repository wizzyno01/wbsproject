﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Areas.WBS.Models;
using WBSProject.Data;

namespace WBSProject.Areas.WBS.Controllers
{
    [Area(Globals.Areas.WBS)]
    [Authorize(Policy = "AdminOnly")]
    public class ReportsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public ReportsController(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> ApplicationsByLGA()
        {
            var applicantInfoQuery = _context.ApplicantInfos
                .Include(a => a.State)
                .Include(a => a.LGA)
                .Include(a => a.Ward)
                .Where(a => a.LGAId != null);

            var applicationInfos = await applicantInfoQuery
                .OrderBy(a => a.Surname)
                .ThenBy(a => a.FirstName)
                .ThenBy(a => a.OtherName)
                .ToListAsync();

            var applicantInfosByLga = applicationInfos.GroupBy(a => new { a.LGAId, a.LGA.Name }).Select(ag => new
            {
                ag.Key.LGAId,
                ag.Key.Name,
                applications = ag.ToList()
            }).ToList();

            var model = applicantInfosByLga.OrderBy(a => a.Name).Select(a => new ApplicationsReportByLGA
            {
                LgaID = a.LGAId,
                LgaName = a.Name,
                Applicants = _mapper.Map<List<ApplicantStatus>>(a.applications)
            }).ToList();

            return View(model);
        }

        public async Task<IActionResult> ApplicationsDetailByLGA(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var applicantInfoQuery = _context.ApplicantInfos
                .Include(a => a.State)
                .Include(a => a.LGA)
                .Include(a => a.Ward)
                .Where(a => a.LGAId == id);

            var applicationInfos = await applicantInfoQuery
                .OrderBy(a => a.Surname)
                .ThenBy(a => a.FirstName)
                .ThenBy(a => a.OtherName)
                .ToListAsync();

            var applicantInfosByLga = applicationInfos.GroupBy(a => new { a.LGAId, a.LGA.Name }).Select(ag => new
            {
                ag.Key.LGAId,
                ag.Key.Name,
                applications = ag.ToList()
            }).FirstOrDefault();

            var model = new ApplicationsReportByLGA
            {
                LgaID = applicantInfosByLga.LGAId,
                LgaName = applicantInfosByLga.Name,
                Applicants = _mapper.Map<List<ApplicantStatus>>(applicantInfosByLga.applications)
            };

            return View(model);
        }
    }
}

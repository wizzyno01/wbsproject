﻿using AutoMapper;
using FormHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Areas.WBS.Models;
using WBSProject.Data;
using WBSProject.Models;
using WBSProject.Models.EF;

namespace WBSProject.Areas.WBS.Controllers
{
    [Area(Globals.Areas.WBS)]
    [Authorize(Policy = "AdminOnly")]
    public class MessageTemplatesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly AppMessages _messageTemplates;
        private readonly IMapper _mapper;

        public MessageTemplatesController(ApplicationDbContext context, AppMessages messageTemplates, IMapper mapper)
        {
            _context = context;
            _messageTemplates = messageTemplates;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var templates = await _context.MessageTemplates.ToListAsync();
            return View(templates);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
                return BadRequest();

            var template = await _context.MessageTemplates.SingleOrDefaultAsync(t => t.Id == id);

            if (template == null)
                return NotFound();

            var model = _mapper.Map<EditMessageTemplate>(template);
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditMessageTemplate model)
        {
            if (ModelState.IsValid)
            {
                var template = await _context.MessageTemplates.SingleAsync(t => t.Id == model.Id);
                template = _mapper.Map(model, template);

                _context.MessageTemplates.Update(template);
                await _context.SaveChangesAsync();

                return FormResult.CreateSuccessResult("Updated message template successfully.", Url.Action(nameof(Index)));
            }
            return View(model);
        }
    }
}

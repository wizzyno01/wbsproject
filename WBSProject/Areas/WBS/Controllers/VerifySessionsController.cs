﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WBSProject.Data;
using WBSProject.Hubs;
using WBSProject.Models.EF;

namespace WBSProject.Areas.WBS.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VerifySessionsController : ControllerBase
    {
        private readonly ApplicationDbContext db;
        private readonly IVerifySessionService service;

        public VerifySessionsController(ApplicationDbContext db, IVerifySessionService service)
        {
            this.db = db;
            this.service = service;
        }

        // GET: api/VerifySessions
        [HttpGet]
        public IQueryable<CaptureSession> GetVerifySessions()
        {
            return db.CaptureSessions;
        }

        // GET: api/VerifySessions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVerifySession(Guid id)
        {
            CaptureSession verifySession = await db.CaptureSessions.FindAsync(id);
            if (verifySession == null)
            {
                return NotFound();
            }

            return Ok(verifySession);
        }

        // PUT: api/VerifySessions/5
        [HttpPut]
        public async Task<IActionResult> PutVerifySession(CaptureSession verifySession)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                int result = 0;
                var _verifySession = await db.CaptureSessions.Include(vs => vs.SessionData)
                    .SingleOrDefaultAsync(vs => vs.Id == verifySession.Id);

                if (_verifySession != null)
                {
                    _verifySession.SessionData.LeftIndexImage = verifySession.SessionData.LeftIndexImage;
                    _verifySession.SessionData.LeftIndexMinutia = verifySession.SessionData.LeftIndexMinutia;

                    result = await db.SaveChangesAsync();
                }

                var hub = new VerifySessionHub(db);
                await hub.NotifyNewVerify(service.Context, verifySession.Id);

                //Hangfire.BackgroundJob.Enqueue<IVerifySessionService>((service) => service.ScanForPatient(verifySession.Id));

                return Ok(result);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VerifySessionExists(verifySession.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // POST: api/VerifySessions
        [HttpPost]
        public async Task<IActionResult> PostVerifySession(CaptureSession verifySession)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CaptureSessions.Add(verifySession);
            int result = 0;
            try
            {
                result = await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VerifySessionExists(verifySession.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return  CreatedAtRoute("DefaultApi", new { id = verifySession.Id }, verifySession);//Ok(result); 
        }

        // DELETE: api/VerifySessions/5
        [HttpDelete]
        public async Task<IActionResult> DeleteVerifySession(Guid id)
        {
            CaptureSession verifySession = await db.CaptureSessions.FindAsync(id);
            if (verifySession == null)
            {
                return NotFound();
            }

            db.CaptureSessions.Remove(verifySession);
            await db.SaveChangesAsync();

            return Ok(verifySession);
        }

        private bool VerifySessionExists(Guid id)
        {
            return db.CaptureSessions.Count(e => e.Id == id) > 0;
        }
    }
}

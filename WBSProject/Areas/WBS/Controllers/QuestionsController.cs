using AutoMapper;
using FormHelper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Areas.WBS.Models;
using WBSProject.Data;
using WBSProject.Domain;
using WBSProject.Models.EF;

namespace WBSProject.Areas.WBS.Controllers
{
    [Area(Globals.Areas.WBS)]
    public class QuestionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        public QuestionsController(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Form(Guid? applicantId, int? step)
        {
            if (applicantId == null)
                return BadRequest();

            var applicantInfo = await _context.ApplicantInfos.SingleOrDefaultAsync(i => i.Id == applicantId);

            if (applicantInfo == null)
                return NotFound();

            var applicantQuestions = await _context.Questioniers.SingleOrDefaultAsync(i => i.ApplicantId == applicantId);

            if(applicantQuestions == null)
            {
                applicantQuestions = new Questionier()
                {
                    Id = Guid.NewGuid(),
                    ApplicantId = applicantInfo.Id,
                    Step = 1
                };

                _context.Questioniers.Add(applicantQuestions);
                await _context.SaveChangesAsync();
            }

             var model = new QuestionierViewModel();

             model = _mapper.Map<QuestionierViewModel>(applicantQuestions);
             model = _mapper.Map<QuestionierViewModel>(applicantInfo);

            ViewData["CurrentSetupStep"] = model.Step;

            if (step != null)
                model.Step = step.Value;
          
            return View(model);
        }

        [FormValidator]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Form(QuestionierViewModel model)
        {
            if (ModelState.IsValid)
            {
                var applicantQuestions = await _context.Questioniers.SingleOrDefaultAsync(i => i.ApplicantId == model.ApplicantId);

                if (applicantQuestions == null)
                    return NotFound();

                int nextStep = applicantQuestions.Step + 1;
                model.Step = model.IsUpdate ? applicantQuestions.Step : nextStep;
                applicantQuestions = _mapper.Map(model, applicantQuestions);

                if(applicantQuestions.Step == 6)
                {
                    var applicantInfo = await _context.ApplicantInfos.SingleAsync(i => i.Id == model.ApplicantId);
                    applicantInfo = _mapper.Map(model, applicantInfo);
                    _context.ApplicantInfos.Update(applicantInfo);
                }

                _context.Questioniers.Update(applicantQuestions);
                await _context.SaveChangesAsync();

                var redirect = model.IsUpdate ? Url.Action("Status", "admin", new { id = model.ApplicantId }) : Url.Action("form", "questions", new { applicantId = model.ApplicantId });

                return FormResult.CreateSuccessResult("Submission successful", redirect);
            }

            return View(model);
        }

    }
}

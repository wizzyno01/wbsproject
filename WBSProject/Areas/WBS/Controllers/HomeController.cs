﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WBSProject.Data;
using WBSProject.Helpers;
using WBSProject.Models.EF;

namespace WBSProject.Areas.WBS.Controllers
{
    [Area(Globals.Areas.WBS)]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext dbContext;
        public HomeController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Dashboard()
        {
            return RedirectToAction("Index", "Dashboard");

            var applicantInfo = await dbContext.ApplicantInfos
                .Include(a => a.State)
                .Include(a => a.LGA)
                .Include(a => a.Ward)
                .FirstOrDefaultAsync(a => a.Username == User.Identity.Name);

            if (applicantInfo == null)
                return RedirectToAction(nameof(Register));

            return View(applicantInfo);
        }

        public async Task<IActionResult> Register()
        {
            return RedirectToAction("Dashboard");

            ViewBag.Genders = SelectListHelper.Genders();
            ViewBag.BusinessStatuses = SelectListHelper.BusinessStatuses();
            ViewBag.BusinessAges = SelectListHelper.BusinessAges();
            ViewBag.EmployeeNumbers = SelectListHelper.EmployeeNumbers();
            ViewBag.States = await dbContext.States();
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(ApplicantInfo applicantInfo)
        {
            return RedirectToAction("Dashboard");

            if (ModelState.IsValid)
            {
                applicantInfo.Id = Guid.NewGuid();
                applicantInfo.Username = User.Identity.Name;
                dbContext.ApplicantInfos.Add(applicantInfo);

                await dbContext.SaveChangesAsync();

                return RedirectToAction(nameof(Dashboard));
            }

            ViewBag.Genders = SelectListHelper.Genders();
            ViewBag.BusinessStatuses = SelectListHelper.BusinessStatuses();
            ViewBag.BusinessAges = SelectListHelper.BusinessAges();
            ViewBag.EmployeeNumbers = SelectListHelper.EmployeeNumbers();
            ViewBag.States = await dbContext.States();
            return View(applicantInfo);
        }

        [HttpPost, AllowAnonymous]
        public async Task<JsonResult> GetLGAs(Guid state)
        {
            var lgas = await dbContext.Lgas.Where(l => l.StateId == state).ToArrayAsync();
            return Json(lgas);
        }

        [HttpPost, AllowAnonymous]
        public async Task<JsonResult> GetWards(Guid lga)
        {
            var wards = await dbContext.Wards.Where(l => l.LgaId == lga).ToArrayAsync();
            return Json(wards);
        }
    }
}

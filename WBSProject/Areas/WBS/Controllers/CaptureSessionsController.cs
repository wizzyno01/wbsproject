﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WBSProject.Data;
using WBSProject.Hubs;
using WBSProject.Models.EF;

namespace WBSProject.Areas.WBS.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CaptureSessionsController : ControllerBase
    {
        private readonly ApplicationDbContext db;
        private readonly ICaptureSessionService service;

        public CaptureSessionsController(ApplicationDbContext db, ICaptureSessionService service)
        {
            this.db = db;
            this.service = service;
        }

        // GET: api/CaptureSessions
        [HttpGet]
        public IQueryable<CaptureSession> GetCaptureSessions()
        {
            return db.CaptureSessions;
        }

        // GET: api/CaptureSessions/5
        [HttpGet("{id}")]
        //[ResponseType(typeof(CaptureSession))]
        public async Task<IActionResult> GetCaptureSession(Guid id)
        {
            CaptureSession captureSession = await db.CaptureSessions.FindAsync(id);
            if (captureSession == null)
            {
                return NotFound();
            }

            return Ok(captureSession);
        }

        // PUT: api/CaptureSessions/5
        [HttpPut]
        //[ResponseType(typeof(int))]
        public async Task<IActionResult> PutCaptureSession(CaptureSession captureSession)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                int result = 0;
                CaptureSession _captureSession = null;
                var hub = new CaptureSessionHub(db);

                switch (captureSession.DataType)
                {
                    case 1:
                         _captureSession = await db.CaptureSessions.Include(cc => cc.SessionData)
                            .SingleOrDefaultAsync(cc => cc.Id == captureSession.Id);

                        if (_captureSession != null)
                        {
                            _captureSession.SessionData.Photograph = captureSession.SessionData.Photograph;
                            _captureSession.State = SessionState.Busy;
                            result = await db.SaveChangesAsync();

                            await hub.NotifyNewCapture(service.Context, captureSession.Id);
                        }
                        break;

                    case 2:
                        _captureSession = await db.CaptureSessions.Include(cc => cc.SessionData)
                            .SingleOrDefaultAsync(cc => cc.Id == captureSession.Id);

                        if (_captureSession != null)
                        {
                            // Template properties hold the minutia from enrollment.
                            _captureSession.SessionData.LeftIndexImage = captureSession.SessionData.LeftIndexImage;
                            _captureSession.SessionData.LeftIndexMinutia = captureSession.SessionData.LeftIndexTemplate;
                            _captureSession.SessionData.LeftThumbImage = captureSession.SessionData.LeftThumbImage;
                            _captureSession.SessionData.LeftThumbMinutia = captureSession.SessionData.LeftThumbTemplate;
                            _captureSession.SessionData.RightIndexImage = captureSession.SessionData.RightIndexImage;
                            _captureSession.SessionData.RightIndexMinutia = captureSession.SessionData.RightIndexTemplate;
                            _captureSession.SessionData.RightThumbImage = captureSession.SessionData.RightThumbImage;
                            _captureSession.SessionData.RightThumbMinutia = captureSession.SessionData.RightThumbTemplate;
                            _captureSession.State = SessionState.Busy;

                            result = await db.SaveChangesAsync();

                            await hub.NotifyNewCapture(service.Context, captureSession.Id);
                        }
                        break;

                    case 3:
                        break;

                    case 4:
                        _captureSession = await db.CaptureSessions.Include(cc => cc.SessionData)
                           .SingleOrDefaultAsync(cc => cc.Id == captureSession.Id);

                        if (_captureSession != null)
                        {
                            _captureSession.SessionData.Documents = captureSession.SessionData.Documents;
                            _captureSession.State = SessionState.Busy;
                            result = await db.SaveChangesAsync();

                            await hub.NotifyNewCaptureDocs(service.Context, captureSession.Id);
                        }
                        break;
                }
                //if (captureSession.DataType == 1)
                //{
                //    var _captureSession = await db.CaptureSessions.Include(cc => cc.SessionData)
                //        .SingleOrDefaultAsync(cc => cc.Id == captureSession.Id);

                //    if (_captureSession != null)
                //    {
                //        _captureSession.SessionData.Photograph = captureSession.SessionData.Photograph;
                //        _captureSession.State = SessionState.Busy;
                //        result = await db.SaveChangesAsync();
                //    }
                //}
                //else
                //{
                //    var _captureSession = await db.CaptureSessions.Include(cc => cc.SessionData)
                //        .SingleOrDefaultAsync(cc => cc.Id == captureSession.Id);

                //    if (_captureSession != null)
                //    {
                //        // Template properties hold the minutia from enrollment.
                //        _captureSession.SessionData.LeftIndexImage = captureSession.SessionData.LeftIndexImage;
                //        _captureSession.SessionData.LeftIndexMinutia = captureSession.SessionData.LeftIndexTemplate;
                //        _captureSession.SessionData.LeftThumbImage = captureSession.SessionData.LeftThumbImage;
                //        _captureSession.SessionData.LeftThumbMinutia = captureSession.SessionData.LeftThumbTemplate;
                //        _captureSession.SessionData.RightIndexImage = captureSession.SessionData.RightIndexImage;
                //        _captureSession.SessionData.RightIndexMinutia = captureSession.SessionData.RightIndexTemplate;
                //        _captureSession.SessionData.RightThumbImage = captureSession.SessionData.RightThumbImage;
                //        _captureSession.SessionData.RightThumbMinutia = captureSession.SessionData.RightThumbTemplate;
                //        _captureSession.State = SessionState.Busy;

                //        result = await db.SaveChangesAsync();
                //    }
                //}

                //var hub = new CaptureSessionHub(db);
                //await hub.NotifyNewCapture(service.Context, captureSession.Id);

                return Ok(result);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CaptureSessionExists(captureSession.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // POST: api/CaptureSessions
        [HttpPost]
        //[ResponseType(typeof(CaptureSession))]
        public async Task<IActionResult> PostCaptureSession(CaptureSession captureSession)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CaptureSessions.Add(captureSession);
            try
            {
                int result = await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CaptureSessionExists(captureSession.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return  CreatedAtRoute("DefaultApi", new { id = captureSession.Id }, captureSession);//Ok(result); 
        }

        // DELETE: api/CaptureSessions/5
        [HttpDelete]
        //[ResponseType(typeof(CaptureSession))]
        public async Task<IActionResult> DeleteCaptureSession(Guid id)
        {
            CaptureSession captureSession = await db.CaptureSessions.FindAsync(id);
            if (captureSession == null)
            {
                return NotFound();
            }

            db.CaptureSessions.Remove(captureSession);
            await db.SaveChangesAsync();

            return Ok(captureSession);
        }

        private bool CaptureSessionExists(Guid id)
        {
            return db.CaptureSessions.Count(e => e.Id == id) > 0;
        }
    }
}

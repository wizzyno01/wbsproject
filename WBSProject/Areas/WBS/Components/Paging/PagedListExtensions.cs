﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WBSProject.Components.Paging
{
    public static class PagedListExtensions
    {
        public static PagedList<T> ToPagedList<T>(this IQueryable<T> query, int page, int pageSize) where T : class
        {
            var result = new PagedList<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = query.Count()
            };

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Entries = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }

        public static Task<PagedList<T>> ToPagedListAsync<T>(this IQueryable<T> query, int page, int pageSize) where T : class
        {
            return Task.Run(() => query.ToPagedList(page, pageSize));
        }


        /// <summary>
        /// Returns the display name for the specified <paramref name="expression"/>
        /// if the current model represents a collection.
        /// </summary>
        /// <param name="htmlHelper">
        /// The <see cref="IHtmlHelper{T}"/> of <see cref="IEnumerable{TModelItem}"/> instance this method extends.
        /// </param>
        /// <param name="expression">An expression to be evaluated against an item in the current model.</param>
        /// <typeparam name="TModelItem">The type of items in the model collection.</typeparam>
        /// <typeparam name="TResult">The type of the <paramref name="expression"/> result.</typeparam>
        /// <returns>A <see cref="string"/> containing the display name.</returns>
        public static string DisplayNameFor<TModelItem, TResult>(
            this IHtmlHelper<IPagedList<TModelItem>> htmlHelper,
            Expression<Func<TModelItem, TResult>> expression)
        {
            if (htmlHelper == null)
            {
                throw new ArgumentNullException(nameof(htmlHelper));
            }

            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            return htmlHelper.DisplayNameForInnerType(expression);
        }
    }
}

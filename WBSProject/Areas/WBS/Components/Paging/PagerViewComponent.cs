﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WBSProject.Components.Paging
{
    public class PagerViewComponent : ViewComponent
    {
        public Task<IViewComponentResult> InvokeAsync(PagedListBase result)
        {
            return Task.FromResult((IViewComponentResult)View("Default", result));
        }
    }
}

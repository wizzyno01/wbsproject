﻿using System.Collections;
using System.Collections.Generic;

namespace WBSProject.Components.Paging
{
    public class PagedList<T> : PagedListBase, IPagedList<T> where T : class
    {
        public IList<T> Entries { get; set; }

        public PagedList()
        {
            Entries = new List<T>();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Entries.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Entries.GetEnumerator();
        }
    }
}

﻿using System.Collections.Generic;

namespace WBSProject.Components.Paging
{
    public interface IPagedList<T> : IEnumerable<T>
    {

    }
}

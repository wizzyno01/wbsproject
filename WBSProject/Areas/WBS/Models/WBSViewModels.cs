﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Models.EF;

namespace WBSProject.Areas.WBS.Models
{
    public class EditMessageTemplate
    {
        public Guid Id { get; set; }

        public string Category { get; set; }
        /// <summary>
        /// A description for the category of the template.
        /// </summary>
        public string Description { get; set; }
        public MessageType Type { get; set; }
        /// <summary>
        /// Subject if template is for email and 
        /// Sender ID if template is for SMS.
        /// </summary>
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}

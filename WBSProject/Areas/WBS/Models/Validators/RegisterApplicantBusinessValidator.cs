﻿using FluentValidation;

namespace WBSProject.Areas.WBS.Models
{
    public class RegisterApplicantBusinessValidator : AbstractValidator<RegisterApplicantBusiness>
    {
        public RegisterApplicantBusinessValidator()
        {
            RuleFor(x => x.BusinessName).NotEmpty().Length(4, 128);
            RuleFor(x => x.NatureOfBusiness).NotEmpty().Length(4, 255);
            RuleFor(x => x.AgeOfBusiness).NotEmpty().MaximumLength(50);
            RuleFor(x => x.BusinessStatus).NotEmpty().MaximumLength(20);
            RuleFor(x => x.RCNumber).NotEmpty().MaximumLength(20).When(x => x.BusinessStatus == "Registered");
            RuleFor(x => x.NumberOfEmployees).NotEmpty().MaximumLength(50);
        }
    }
}

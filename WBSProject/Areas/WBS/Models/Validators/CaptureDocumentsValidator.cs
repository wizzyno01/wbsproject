﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Areas.WBS.Models
{
    public class CaptureDocumentsValidator : AbstractValidator<CaptureDocuments>
    {
        public CaptureDocumentsValidator()
        {
            RuleFor(x => x.Session).NotNull();
            RuleFor(x => x.ApplicantId).NotNull();
            RuleFor(x => x.BusinessName).NotEmpty();
            RuleFor(x => x.FullNames).NotEmpty();
            RuleFor(x => x.Documents).NotEmpty().WithMessage("No documents captured.");
        }
    }
}

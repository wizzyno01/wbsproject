﻿using FluentValidation;
using System.Text.RegularExpressions;

namespace WBSProject.Areas.WBS.Models
{
    public class RegisterApplicantPersonalValidator : AbstractValidator<RegisterApplicantPersonal>
    {
        public RegisterApplicantPersonalValidator()
        {
            RuleFor(x => x.Surname).NotEmpty().Length(2, 50);
            RuleFor(x => x.FirstName).NotEmpty().Length(2, 50);
            RuleFor(x => x.OtherName).MaximumLength(50);
            RuleFor(x => x.Gender).NotEmpty().MaximumLength(10);
            RuleFor(x => x.BirthDate).NotNull();
            RuleFor(x => x.PhoneNumber).NotEmpty().MaximumLength(20).Custom((phoneNumber, context) =>
            {
                var regex = new Regex(@"^\(?([0-9]{2,4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$");
                if (!regex.IsMatch(phoneNumber))
                    context.AddFailure("Phone number is not valid.");
            });
            RuleFor(x => x.Email).NotEmpty().EmailAddress();
            RuleFor(x => x.Address).NotEmpty().Length(4, 255);
        }
    }
}

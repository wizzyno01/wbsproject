﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Areas.WBS.Models
{
    public class EditMessageTemplateValidator : AbstractValidator<EditMessageTemplate>
    {
        public EditMessageTemplateValidator()
        {
            RuleFor(x => x.Category).NotEmpty().MaximumLength(128);
            RuleFor(x => x.Description).NotEmpty().MaximumLength(255);
            RuleFor(x => x.Subject).MaximumLength(128);
        }
    }
}

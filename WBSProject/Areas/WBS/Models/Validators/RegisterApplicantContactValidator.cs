﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Areas.WBS.Models
{
    public class RegisterApplicantContactValidator : AbstractValidator<RegisterApplicantContact>
    {
        public RegisterApplicantContactValidator()
        {
            RuleFor(x => x.BusinessAddress).NotEmpty().Length(4, 255);
            RuleFor(x => x.StateId).NotNull();
            RuleFor(x => x.LGAId).NotNull();
            RuleFor(x => x.WardId).NotNull();
        }
    }
}

﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Areas.WBS.Models
{
    public class RegisterApplicantGrantValidator : AbstractValidator<RegisterApplicantGrant>
    {
        public RegisterApplicantGrantValidator()
        {
            var dli3_3 = Guid.Parse("C7563F63-28E0-4051-86F7-07FDFD22FF41");    // ICT DLI

            RuleFor(x => x.NetWorthOfGoods).NotNull();
            RuleFor(x => x.CashInBank).NotNull();
            RuleFor(x => x.DistanceFromCustomers).NotEmpty();
            RuleFor(x => x.OwnersGender).NotEmpty();
            RuleFor(x => x.DescribeComputerSkills).NotEmpty().When(x => x.DLIId == dli3_3);
            RuleFor(x => x.DescribeRecordKeeping).NotEmpty().When(x => x.HaveRecordKeeping);
        }
    }
}

using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Areas.WBS.Models.Validators
{
    public class QuestionValidator : AbstractValidator<QuestionierViewModel>
    {
        public QuestionValidator()
        {
            /// <summary>
            /// Step 1 Validations
            /// </summary> 

            RuleFor(x => x.StateOfOrigin).NotEmpty().When(x => x.Step == 1);
            RuleFor(x => x.LGAOfOrigin).NotEmpty().When(x => x.Step == 1);
            RuleFor(x => x.NIN).NotEmpty().Length(11, 11).When(x => x.Step == 1);
            RuleFor(x => x.BVN).NotEmpty().Length(11, 11).When(x => x.Step == 1);
            RuleFor(x => x.BankName).NotEmpty().When(x => x.Step == 1);
            RuleFor(x => x.AccountName).NotEmpty().When(x => x.Step == 1);
            RuleFor(x => x.AccountNumber).NotEmpty().Length(10, 10).When(x => x.Step == 1);

            /// <summary>
            /// Step 2 Validations
            /// </summary>

            RuleFor(x => x.NumberOfStaff).NotEmpty().When(x => x.Step == 2);
            RuleFor(x => x.DateOfChangeInNumberOfStaff).NotEmpty().When(x => x.Step == 2 && x.ChangeInNumberOfStaff);
            RuleFor(x => x.WhyMoreStaff).NotEmpty().When(x => x.Step == 2 && !x.IsStaffAdequate);
            RuleFor(x => x.StaffSalary).NotEmpty().When(x => x.Step == 2);
            RuleFor(x => x.ReasonForChangeInStaffSalary).NotEmpty().When(x => x.Step == 2 && x.ChangeInStaffSalary);
            RuleFor(x => x.DateOfChangeInStaffSalary).NotEmpty().When(x => x.Step == 2 && x.ChangeInStaffSalary);

            /// <summary>
            /// Step 3 Validations
            /// </summary>

            RuleFor(x => x.MonthlyIncome).NotEmpty().When(x => x.Step == 3);
            RuleFor(x => x.ReasonForChangeInMonthlyIncome).NotEmpty().When(x => x.Step == 3 && x.ChangeInMonthlyIncome);
            RuleFor(x => x.DateofChangeInMonthlyIncome).NotEmpty().When(x => x.Step == 3 && x.ChangeInMonthlyIncome);
            RuleFor(x => x.ReasonForChangeInProductionVolume).NotEmpty().When(x => x.Step == 3 && x.ChangeInProductionVolume);
            RuleFor(x => x.DateOfChangeInProductionVolume).NotEmpty().When(x => x.Step == 3 && x.ChangeInProductionVolume);

            /// <summary>
            /// Step 4 Validations
            /// </summary>

            RuleFor(x => x.ICTToolInUse).NotEmpty().When(x => x.Step == 4);
            RuleFor(x => x.ICTToolNeeded).NotEmpty().When(x => x.Step == 4);
            RuleFor(x => x.CurrentMeansOfAdvertisement).NotEmpty().When(x => x.Step == 4);
            RuleFor(x => x.MethodOfReceivingPayment).NotEmpty().When(x => x.Step == 4);

            /// <summary>
            /// Step 5 Validations
            /// </summary>

            RuleFor(x => x.AverageCostOfPHEDBills).NotEmpty().When(x => x.Step == 5);
            RuleFor(x => x.AverageCostOfWaterBills).NotEmpty().When(x => x.Step == 5);
            RuleFor(x => x.CostOfMonthlyRent).NotEmpty().When(x => x.Step == 5 && x.IsPayRent);
            RuleFor(x => x.RequirePowerOrLight).NotEmpty().When(x => x.Step == 5);
            RuleFor(x => x.AlternatePower).NotEmpty().When(x => x.Step == 4);
            RuleFor(x => x.DateAlternatePowerWasinitialized).NotEmpty().When(x => x.Step == 5);

            /// <summary>
            /// Step 5 Validations
            /// </summary>

            RuleFor(x => x.WebsiteURL).NotEmpty().When(x => x.Step == 6 && x.HasWebsite);
            RuleFor(x => x.RegistrationCompany).NotEmpty().When(x => x.Step == 6);
            RuleFor(x => x.RCNumber).NotEmpty().When(x => x.Step == 6);
            RuleFor(x => x.COVID19EffectOnBusiness).NotEmpty().When(x => x.Step == 6);

        }

    }
}

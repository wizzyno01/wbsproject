﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Areas.WBS.Models
{
    public class RegisterApplicantPersonal
    {
        public Guid? Id { get; set; }

        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string OtherName { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }

    public class RegisterApplicantBusiness
    {
        public Guid Id { get; set; }

        public string BusinessName { get; set; }
        public string NatureOfBusiness { get; set; }
        public string AgeOfBusiness { get; set; }
        /// <summary>
        /// Whether it is registered or not
        /// </summary>
        public string BusinessStatus { get; set; }
        public string RCNumber { get; set; }
        /// <summary>
        /// Employee number in range e.g 10-50
        /// </summary>
        public string NumberOfEmployees { get; set; }
    }

    public class RegisterApplicantContact
    {
        public Guid Id { get; set; }

        public string BusinessAddress { get; set; }
        public Guid? StateId { get; set; }
        public Guid? LGAId { get; set; }
        public Guid? WardId { get; set; }
    }

    public class RegisterApplicantGrant
    {
        public Guid Id { get; set; }
        public Guid ApplicantId { get; set; }
        public string FullNames { get; set; }
        public string BusinessName { get; set; }

        public Guid? DLIId { get; set; }

        /// <summary>
        /// How much goods does your business have in stock.
        /// </summary>
        public decimal? NetWorthOfGoods { get; set; }
        /// <summary>
        /// How much cash does your business have in the bank.
        /// </summary>
        public decimal? CashInBank { get; set; }

        /// <summary>
        /// How far from location do customers come ..?
        /// </summary>
        public string DistanceFromCustomers { get; set; }
        public bool HaveWater { get; set; }
        public bool HaveElectricity { get; set; }
        public bool HaveRoad { get; set; }
        public bool HaveMedicalCare { get; set; }
        public bool HaveNetwork { get; set; }

        public string OwnersGender { get; set; }
        /// <summary>
        /// Age of promoter (Should give considerations to older persons who are more vulnerable)
        /// </summary>
        public string PromotersAge { get; set; }

        public string DescribeComputerSkills { get; set; }
        public bool OwnSmartPhone { get; set; }
        public bool HaveFacebook { get; set; }
        public bool HaveTwitter { get; set; }
        public bool HaveWhatsApp { get; set; }
        public bool HaveInternet { get; set; }
        public bool HaveRecordKeeping { get; set; }
        /// <summary>
        /// If you have record keeping, describe eg. Manual, Digital, Mental
        /// </summary>
        public string DescribeRecordKeeping { get; set; }

        /// <summary>
        /// How many people living in the household
        /// </summary>
        public string NoOfPersonsInHousehold { get; set; }
        /// <summary>
        /// What is the contribution of the business to livelihood
        /// </summary>
        public string ContributionToLivelihood { get; set; }
    }

    public class RegisterEmployee
    {
        public string Names { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }

        public Guid GrantId { get; set; }
    }

    public class RegistrationSummary
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string BusinessName { get; set; }
    }
}

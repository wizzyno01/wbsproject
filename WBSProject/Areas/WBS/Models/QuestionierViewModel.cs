using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Areas.WBS.Models
{
    public class QuestionierViewModel
    {
        public Guid ApplicantId { get; set; }
        public int Step { get; set; }

        public bool IsUpdate { get; set; }


        /// <summary>
        /// Step 1 
        /// </summary>

        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string BVN { get; set; }
        public string NIN { get; set; }
        public string StateOfOrigin { get; set; }
        public string LGAOfOrigin { get; set; }

        /// <summary>
        /// Step 2
        /// </summary>

        public int? NumberOfStaff { get; set; }
        public bool ChangeInNumberOfStaff { get; set; }
        public DateTime? DateOfChangeInNumberOfStaff { get; set; }
        public bool IsStaffAdequate { get; set; }
        public string WhyMoreStaff { get; set; }

        public decimal? StaffSalary { get; set; }
        public bool ChangeInStaffSalary { get; set; }
        public string ReasonForChangeInStaffSalary { get; set; }
        public DateTime? DateOfChangeInStaffSalary { get; set; }


        /// <summary>
        /// Step 3
        /// </summary>

        public bool ChangeInProductionVolume { get; set; }
        public string ReasonForChangeInProductionVolume { get; set; }

        public DateTime? DateOfChangeInProductionVolume { get; set; }

        public decimal MonthlyIncome { get; set; }
        public bool ChangeInMonthlyIncome { get; set; }
        public string ReasonForChangeInMonthlyIncome { get; set; }
        public DateTime DateofChangeInMonthlyIncome { get; set; }

        /// <summary>
        /// Step 4
        /// </summary>

        public string ICTToolInUse { get; set; }
        public string ICTToolNeeded { get; set; }
        public string CurrentMeansOfAdvertisement { get; set; }
        public bool IsCurrentMeansOfAdvertisementAdequate { get; set; }
        public string MethodOfReceivingPayment { get; set; }

        /// <summary>
        /// Step 5
        /// </summary>

        public decimal? AverageCostOfPHEDBills { get; set; }
        public decimal? AverageCostOfWaterBills { get; set; }
        public bool IsPayRent { get; set; }
        public decimal? CostOfMonthlyRent { get; set; }
        public string RequirePowerOrLight { get; set; }
        public string AlternatePower { get; set; }
        public DateTime? DateAlternatePowerWasinitialized { get; set; }

        /// <summary>
        /// Step 6
        /// </summary>

        public bool HasWebsite { get; set; }
        public string WebsiteURL { get; set; }
        public string FacebookURL { get; set; }
        public string LinkedInURL { get; set; }
        public string TwitterURL { get; set; }
        public string RegistrationCompany { get; set; }
        public string BusinessStatus { get { return "Registered"; } set { } }
        public string RCNumber { get; set; }

        public bool HasAutomatedInvoicingSystem { get; set; }
        public string COVID19EffectOnBusiness { get; set; }

    }

}

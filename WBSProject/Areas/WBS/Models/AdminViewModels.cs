﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Models.EF;

namespace WBSProject.Areas.WBS.Models
{
    public class CaptureBiometrics
    {
        public Guid ApplicantId { get; set; }
        public Guid Session { get; set; }
        public string FullNames { get; set; }

        public string Photo { get; set; }
        public string RightThumbImage { get; set; }
        public string RightIndexImage { get; set; }
        public string LeftThumbImage { get; set; }
        public string LeftIndexImage { get; set; }
        public string RightThumbMinutia { get; set; }
        public string RightIndexMinutia { get; set; }
        public string LeftThumbMinutia { get; set; }
        public string LeftIndexMinutia { get; set; }
        public string Signature { get; set; }
    }

    public class CaptureDocuments
    {
        public Guid ApplicantId { get; set; }
        public Guid Session { get; set; }
        public string FullNames { get; set; }
        public string BusinessName { get; set; }

        public string Documents { get; set; }
    }

    public class BiometricStatus
    {
        public byte[] Photo { get; set; }
        public byte[] RightThumbImage { get; set; }
        public byte[] RightIndexImage { get; set; }
        public byte[] LeftThumbImage { get; set; }
        public byte[] LeftIndexImage { get; set; }
    }

    public class ApplicantStatus
    {
        public Guid Id { get; set; }

        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string OtherName { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }

        public string BusinessName { get; set; }
        public string NatureOfBusiness { get; set; }
        public string AgeOfBusiness { get; set; }
        /// <summary>
        /// Whether it is registered or not
        /// </summary>
        public string BusinessStatus { get; set; }
        public string RCNumber { get; set; }
        /// <summary>
        /// Employee number in range e.g 10-50
        /// </summary>
        public string NumberOfEmployees { get; set; }

        public string BusinessAddress { get; set; }
        public string State { get; set; }
        public string LGA { get; set; }
        public string Ward { get; set; }

        public string FullNames => $"{Surname}, {FirstName} {OtherName}";
    }
    public class GrantStatus
    {
        public Guid Id { get; set; }

        /// <summary>
        /// How much goods does your business have in stock.
        /// </summary>
        public decimal? NetWorthOfGoods { get; set; }
        /// <summary>
        /// How much cash does your business have in the bank.
        /// </summary>
        public decimal? CashInBank { get; set; }

        /// <summary>
        /// How far from location do customers come ..?
        /// </summary>
        public string DistanceFromCustomers { get; set; }
        public bool HaveWater { get; set; }
        public bool HaveElectricity { get; set; }
        public bool HaveRoad { get; set; }
        public bool HaveMedicalCare { get; set; }
        public bool HaveNetwork { get; set; }

        public string OwnersGender { get; set; }
        /// <summary>
        /// Age of promoter (Should give considerations to older persons who are more vulnerable)
        /// </summary>
        public string PromotersAge { get; set; }

        public string DescribeComputerSkills { get; set; }
        public bool OwnSmartPhone { get; set; }
        public bool HaveFacebook { get; set; }
        public bool HaveTwitter { get; set; }
        public bool HaveWhatsApp { get; set; }
        public bool HaveInternet { get; set; }
        public bool HaveRecordKeeping { get; set; }
        /// <summary>
        /// If you have record keeping, describe eg. Manual, Digital, Mental
        /// </summary>
        public string DescribeRecordKeeping { get; set; }

        /// <summary>
        /// How many people living in the household
        /// </summary>
        public string NoOfPersonsInHousehold { get; set; }
        /// <summary>
        /// What is the contribution of the business to livelihood
        /// </summary>
        public string ContributionToLivelihood { get; set; }

        public string DLI { get; set; }
    }
    public class ApplicationStatus
    {
        public BiometricStatus Biometric { get; set; }
        public ApplicantStatus Applicant { get; set; }
        public GrantStatus Grant { get; set; }
    }

    public class ApplicationsReportByLGA
    {
        public Guid? LgaID { get; set; }
        [Display(Name = "LGA")]
        public string LgaName { get; set; }
        public List<ApplicantStatus> Applicants { get; set; }

        public int? Males => Applicants?.Where(a => "male".Equals(a.Gender, StringComparison.OrdinalIgnoreCase)).Count();
        public int? Females => Applicants?.Where(a => "female".Equals(a.Gender, StringComparison.OrdinalIgnoreCase)).Count();
        [Display(Name = "Total")]
        public int? TotalApplications => Applicants?.Count;
    }
    
    public class LGAData
    {
        public string Name { get; set; }
        public Guid? Id { get; set; }
    }

    public class Gender
    {
        public string Value { get; set; }
        public string DisplayName { get; set; }
    }
    
}

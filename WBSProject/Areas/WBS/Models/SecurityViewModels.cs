﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Areas.WBS.Models
{
    public class SignUpViewModel
    {
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required, Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required, StringLength(100, MinimumLength = 6), DataType(DataType.Password)]
        public string Password { get; set; }
        [Required, StringLength(100, MinimumLength = 6), DataType(DataType.Password), Display(Name = "Confirm Password")]
        [Compare(nameof(Password), ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterViewModel
    {
        public int UserNo { get; set; }

        public string Title { get; set; }
        [Required, StringLength(50), Display(Name = "Surname")]
        public string LastName { get; set; }
        [Required, StringLength(50), Display(Name = "First Name")]
        public string FirstName { get; set; }
        [StringLength(50), Display(Name = "Other Name")]
        public string OtherName { get; set; }
        [Required, StringLength(10)]
        public string Gender { get; set; }
        [Required]
        public DateTime? Birthday { get; set; }

        [Required, StringLength(255)]
        public string Address { get; set; }
        [Required, StringLength(50), Display(Name = "Phone No.")]
        public string PhoneNo { get; set; }
        [Required, EmailAddress, StringLength(50), Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        public Guid? State { get; set; }
        [Required]
        public Guid? LGA { get; set; }
        [Required]
        public Guid? Ward { get; set; }
    }
}

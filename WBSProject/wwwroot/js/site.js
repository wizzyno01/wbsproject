﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var showLoading = true;
var promptUnload = false;

$(function () {
    $("#loaderbody").addClass('d-none');

    $(document).bind('ajaxStart', function () {
        if (showLoading) {
            $("#loaderbody").removeClass('d-none');
        }
    }).bind('ajaxStop', function () {
        if (showLoading) {
            $("#loaderbody").addClass('d-none');
        }
    });

    window.addEventListener('beforeunload', function (e) {
        if (promptUnload) {
            // Cancel the event
            e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
            // Chrome requires returnValue to be set
            e.returnValue = '';
        } else {
            // the absence of a returnValue property on the event will guarantee the browser unload happens
            delete e['returnValue'];
        }
    });
});

showInPopup = (url, title) => {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (res) {
            $('#form-modal .modal-body').html(res).find('select.mdb-select').trigger('dynamic-load');
            $('#form-modal .modal-title').html(title);
            $('#form-modal').modal('show');
        }
    });
}

initializeSelect = (ele) => {
    $(ele).materialSelect();
}

prepareSelect2 = (ele) => {
    var url = $(ele).data('url');
    $(ele).select2({
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            width: 'resolve',
            data: function (params) {
                showLoading = false;
                return params;
            },
            processResults: function (data, params) {
                showLoading = true;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * data.pageSize) < data.totalItems
                    }
                };
            },
            width: 'resolve'
        },
        placeholder: 'Select Map-Axis'
    });
}

jQueryAjaxPager = (url, targetId) => {
    if (url == '' || targetId == '') {
        return;
    }

    $.ajax({
        type: 'GET',
        url: url,
        success: function (res) {
            $('#' + targetId).html(res.html);
            promptUnload = false;
        }
    });
}

jQueryAjaxPost = form => {
    try {
        $.ajax({
            type: 'POST',
            url: form.action,
            data: new FormData(form),
            contentType: false,
            processData: false,
            success: function (res) {
                if (res.isValid) {
                    $('#view-all').html(res.html).find('select.mdb-select').trigger('dynamic-load');
                    $('#form-modal .modal-body').html('');
                    $('#form-modal .modal-title').html('');
                    $('#form-modal').modal('hide');
                    promptUnload = false;

                }
                else
                    $('#form-modal .modal-body').html(res.html);

                if (res.promptUnload !== 'undefined') {
                    promptUnload = true;
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    } catch (ex) {
        console.log(ex);
    }

    //to prevent default form submit event
    return false;
}

jQueryAjaxDelete = form => {
    if (confirm('Do you want to delete this record ?')) {
        try {
            $.ajax({
                type: 'POST',
                url: form.action,
                data: new FormData(form),
                contentType: false,
                processData: false,
                success: function (res) {
                    $('#view-all').html(res.html).find('select.mdb-select').trigger('dynamic-load');
                    promptUnload = false;
                },
                error: function (err) {
                    console.log(err);
                }
            });
        } catch (ex) {
            console.log(ex);
        }
    }

    //prevent default form submit event
    return false;
}

mapManager = options => {
    this.latitude = 0;
};
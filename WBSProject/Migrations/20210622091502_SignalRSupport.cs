﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WBSProject.Migrations
{
    public partial class SignalRSupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BiometricInfos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ApplicantId = table.Column<Guid>(nullable: false),
                    FullName = table.Column<string>(nullable: true),
                    Photo = table.Column<byte[]>(nullable: true),
                    RightThumbTemplate = table.Column<byte[]>(nullable: true),
                    RightThumbImage = table.Column<byte[]>(nullable: true),
                    RightIndexTemplate = table.Column<byte[]>(nullable: true),
                    RightIndexImage = table.Column<byte[]>(nullable: true),
                    LeftThumbTemplate = table.Column<byte[]>(nullable: true),
                    LeftThumbImage = table.Column<byte[]>(nullable: true),
                    LeftIndexTemplate = table.Column<byte[]>(nullable: true),
                    LeftIndexImage = table.Column<byte[]>(nullable: true),
                    Signature = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BiometricInfos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CaptureSessions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaptureSessions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CaptureSessionData",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Photograph = table.Column<byte[]>(nullable: true),
                    RightThumbMinutia = table.Column<byte[]>(nullable: true),
                    RightThumbImage = table.Column<byte[]>(nullable: true),
                    RightIndexMinutia = table.Column<byte[]>(nullable: true),
                    RightIndexImage = table.Column<byte[]>(nullable: true),
                    LeftThumbMinutia = table.Column<byte[]>(nullable: true),
                    LeftThumbImage = table.Column<byte[]>(nullable: true),
                    LeftIndexMinutia = table.Column<byte[]>(nullable: true),
                    LeftIndexImage = table.Column<byte[]>(nullable: true),
                    Signature = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaptureSessionData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaptureSessionData_CaptureSessions_Id",
                        column: x => x.Id,
                        principalTable: "CaptureSessions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BiometricInfos");

            migrationBuilder.DropTable(
                name: "CaptureSessionData");

            migrationBuilder.DropTable(
                name: "CaptureSessions");
        }
    }
}

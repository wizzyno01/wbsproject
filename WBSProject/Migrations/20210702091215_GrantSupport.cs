﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WBSProject.Migrations
{
    public partial class GrantSupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DLIs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DLIs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GrantInfos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ApplicantId = table.Column<Guid>(nullable: false),
                    FullNames = table.Column<string>(nullable: true),
                    BusinessName = table.Column<string>(nullable: true),
                    NetWorthOfGoods = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    CashInBank = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    DistanceFromCustomers = table.Column<string>(nullable: true),
                    HaveWater = table.Column<bool>(nullable: false),
                    HaveElectricity = table.Column<bool>(nullable: false),
                    HaveRoad = table.Column<bool>(nullable: false),
                    HaveMedicalCare = table.Column<bool>(nullable: false),
                    HaveNetwork = table.Column<bool>(nullable: false),
                    OwnersGender = table.Column<string>(maxLength: 10, nullable: true),
                    PromotersAge = table.Column<string>(maxLength: 50, nullable: true),
                    DescribeComputerSkills = table.Column<string>(maxLength: 255, nullable: true),
                    OwnSmartPhone = table.Column<bool>(nullable: false),
                    HaveFacebook = table.Column<bool>(nullable: false),
                    HaveTwitter = table.Column<bool>(nullable: false),
                    HaveWhatsApp = table.Column<bool>(nullable: false),
                    HaveInternet = table.Column<bool>(nullable: false),
                    HaveRecordKeeping = table.Column<bool>(nullable: false),
                    DescribeRecordKeeping = table.Column<string>(maxLength: 50, nullable: true),
                    NoOfPersonsInHousehold = table.Column<string>(maxLength: 50, nullable: true),
                    ContributionToLivelihood = table.Column<string>(maxLength: 50, nullable: true),
                    DLIId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GrantInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GrantInfos_DLIs_DLIId",
                        column: x => x.DLIId,
                        principalTable: "DLIs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    Names = table.Column<string>(maxLength: 128, nullable: false),
                    Gender = table.Column<string>(maxLength: 10, nullable: false),
                    Age = table.Column<string>(maxLength: 50, nullable: false),
                    GrantId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeeInfo_GrantInfos_GrantId",
                        column: x => x.GrantId,
                        principalTable: "GrantInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeInfo_GrantId",
                table: "EmployeeInfo",
                column: "GrantId");

            migrationBuilder.CreateIndex(
                name: "IX_GrantInfos_DLIId",
                table: "GrantInfos",
                column: "DLIId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeInfo");

            migrationBuilder.DropTable(
                name: "GrantInfos");

            migrationBuilder.DropTable(
                name: "DLIs");
        }
    }
}

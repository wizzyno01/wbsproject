﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WBSProject.Migrations
{
    public partial class DocumentsAndGpsSupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Documents",
                table: "CaptureSessionData",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ApplicantId = table.Column<Guid>(nullable: false),
                    FullNames = table.Column<string>(nullable: true),
                    BusinessName = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    Content = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeoLocations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ApplicantId = table.Column<Guid>(nullable: false),
                    BusinessName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Longitude = table.Column<double>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    LandMark = table.Column<string>(nullable: true),
                    FrontView = table.Column<byte[]>(nullable: true),
                    LeftSideView = table.Column<byte[]>(nullable: true),
                    RightSideView = table.Column<byte[]>(nullable: true),
                    BackView = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeoLocations", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "GeoLocations");

            migrationBuilder.DropColumn(
                name: "Documents",
                table: "CaptureSessionData");
        }
    }
}

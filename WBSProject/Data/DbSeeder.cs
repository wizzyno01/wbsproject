﻿using DynamicReader;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WBSProject.Models.EF;
using WBSProject.Models.Identity;

namespace WBSProject.Data
{
    public class DbSeeder : IDbSeeder
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;
        public DbSeeder(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public void InitializeUser()
        {
            if (userManager.FindByEmailAsync("wbsadmin@gmail.com").GetAwaiter().GetResult() == null)
            {
                var user = new ApplicationUser
                {
                    FirstName = "System",
                    LastName = "Admin",
                    UserName = "wbsadmin@gmail.com",
                    Email = "wbsadmin@gmail.com",
                    Type = ApplicationUserType.Admin,
                    IsSuperUser = true,
                    EmailConfirmed = true,
                    LockoutEnabled = false
                };

                var identityUserResult = userManager.CreateAsync(user, "ihms@2020").GetAwaiter().GetResult();

                if (roleManager.FindByNameAsync("Admin").GetAwaiter().GetResult() == null)
                {
                    var role = new ApplicationRole("Admin", "Administrative Role");

                    var identityRoleResult = roleManager.CreateAsync(role).GetAwaiter().GetResult();

                    if (identityUserResult.Succeeded && identityRoleResult.Succeeded)
                    {
                        userManager.AddToRoleAsync(user, role.Name).GetAwaiter().GetResult();
                        if (user.IsSuperUser)
                        {
                            var claim = new Claim(ClaimTypes.Role, "SuperUser");
                            userManager.AddClaimAsync(user, claim).GetAwaiter().GetResult(); ;
                        }

                    }

                }

                if (roleManager.FindByNameAsync("Guest").GetAwaiter().GetResult() == null)
                {
                    var role = new ApplicationRole("Guest", "Guest Role");
                    var result = roleManager.CreateAsync(role).GetAwaiter().GetResult();

                    if (result.Succeeded)
                    {
                        // Handle any other Guest Role task.
                    }
                }
            }
        }

        public void InitializeData()
        {
        }

        public void SeedFromFile(string relativeFilePath)
        {
            if (string.IsNullOrEmpty(relativeFilePath))
                return;

            var provider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            var fileInfo = provider.GetFileInfo(relativeFilePath);

            if (!fileInfo.Exists)
                return;

            using var reader = new ExcelReader(fileInfo.CreateReadStream());
            foreach (var worksheet in reader.Worksheets)
            {
                var key = worksheet.Name;

                switch (key)
                {
                    case "DLIs":
                        if (context.DLIs.Any())
                            continue;

                        var dlis = worksheet.Read(cell => cell.Value != null,
                            (cell) => new DLI
                            {
                                Id = Guid.Parse(cell.Value.ToString()),
                                Name = cell.Offset(0, 1).Value.StringCast(),
                                Description = cell.Offset(0, 2).Value.StringCast()
                            }).ToList();
                        context.DLIs.AddRange(dlis);
                        break;

                    case "States":
                        if (context.States.Any())
                            continue;

                        var states = worksheet.Read(cell => cell.Value != null,
                            (cell) => new State
                            {
                                Id = Guid.Parse(cell.Value.ToString()),
                                Name = cell.Offset(0, 1).Value.StringCast()
                            }).ToList();
                        context.States.AddRange(states);
                        break;

                    case "LGAs":
                        if (context.Lgas.Any())
                            continue;

                        var lgas = worksheet.Read(cell => cell.Value != null,
                            (cell) => new Lga
                            {
                                Id = Guid.Parse(cell.Value.ToString()),
                                Name = cell.Offset(0, 1).Value.StringCast(),
                                District = cell.Offset(0, 2).Value.StringCast(),
                                StateId = Guid.Parse(cell.Offset(0, 3).Value.ToString())

                            });

                        context.Lgas.AddRange(lgas);
                        break;

                    case "Wards":
                        if (context.Wards.Any())
                            continue;

                        var wards = worksheet.Read(cell => cell.Value != null,
                            (cell) => new Ward()
                            {
                                Id = Guid.Parse(cell.Value.ToString()),
                                Name = cell.Offset(0, 1).Value.StringCast(),
                                LgaId = Guid.Parse(cell.Offset(0, 2).Value.ToString())
                            });

                        context.Wards.AddRange(wards);
                        break;

                }
                context.SaveChanges();
            }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WBSProject.Models.EF;
using WBSProject.Models.Identity;
using WBSProject.Security;

namespace WBSProject.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        private readonly CurrentUser _user;
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, CurrentUser user)
            : base(options)
        {
            _user = user;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicantInfo>()
                .HasOne(p => p.State)
                .WithMany()
                .HasForeignKey(p => p.StateId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<ApplicantInfo>()
                .HasOne(p => p.LGA)
                .WithMany()
                .HasForeignKey(p => p.LGAId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<ApplicantInfo>()
                .HasOne(p => p.Ward)
                .WithMany()
                .HasForeignKey(p => p.WardId)
                .OnDelete(DeleteBehavior.NoAction);
                
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            this.Audit(_user.Username);
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            this.Audit(_user.Username);
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public DbSet<PTK> PTKs { get; set; }
        public DbSet<OS> OS { get; set; }
        public DbSet<DLI> DLIs { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Lga> Lgas { get; set; }
        public DbSet<Ward> Wards { get; set; }
        public DbSet<Questionier> Questioniers { get; set; }
        public DbSet<CaptureSession> CaptureSessions { get; set; }
        public DbSet<ApplicantInfo> ApplicantInfos { get; set; }
        public DbSet<GrantInfo> GrantInfos { get; set; }
        public DbSet<BiometricInfo> BiometricInfos { get; set; }
        public DbSet<MessageTemplate> MessageTemplates { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<GeoLocation> GeoLocations { get; set; }
    }
}

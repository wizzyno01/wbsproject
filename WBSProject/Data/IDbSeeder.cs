﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Data
{
    public interface IDbSeeder
    {
        void InitializeData();
        void InitializeUser();
        void SeedFromFile(string relativeFilePath);
    }
}

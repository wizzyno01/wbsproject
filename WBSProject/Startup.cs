using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using WBSProject.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WBSProject.Models.Identity;
using ReturnTrue.AspNetCore.Identity.Anonymous;
using WebkitFrameworkCore;
using WBSProject.Security;
using Microsoft.AspNetCore.Identity.UI.Services;
using WBSProject.Email;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using FluentValidation.AspNetCore;
using FormHelper;
using WBSProject.Biometrics;
using WBSProject.Helpers;
using WBSProject.Hubs;
using WBSProject.Sms;
using WebUtility.Components;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.FileProviders;
using WBSProject.Domain.SharedComponent;

namespace WBSProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.SignIn.RequireConfirmedAccount = true;
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;

            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddClaimsPrincipalFactory<ApplicationUserClaimsPrincipalFactory>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Identity/Account/LogIn";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
            });
            services.AddAuthentication().AddCookie(options =>
            {
                options.LoginPath = "/Identity/Account/LogIn";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy("SuperUser", policy => policy.RequireClaim(Globals.ClaimTypes.SuperUser, "true"));
                options.AddPolicy("AdminOnly", policy => policy.RequireClaim(Globals.ClaimTypes.UserType, "admin"));
                options.AddPolicy("AdminAndAudit", policy => policy.RequireClaim(Globals.ClaimTypes.UserType, "admin", "audit"));
            });

            services.AddFluentValidation(config =>
            {
                config.RegisterValidatorsFromAssemblyContaining<Startup>();
                //config.ConfigureClientsideValidation(csConfig =>
                //{
                //    csConfig.Add(typeof(Adapters.RequiredIfValidator<Models.BookingRequest, bool>), (context, rule, validator) =>
                //    {
                //        return new Adapters.RequiredIfClientValidator(rule, validator);
                //    });
                //});
            });
            services.AddAutoMapper(typeof(Startup));

            services.AddSession(options =>
            {
                options.Cookie.Name = ".WBSProject.Session";
                options.IdleTimeout = TimeSpan.FromMinutes(5);
                options.Cookie.IsEssential = true;
            });

            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation()
                .AddFormHelper();

            services.AddRazorPages();
            //services.RegisterWebUtilityComponents();
            services.AddSignalR();
            services.AddScoped<ISharedComponents, SharedComponents>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<CurrentUser>();

            services.AddMemoryCache();

            services.AddScoped<IHttpRuntimeCache, HttpRuntimeCache>();
            services.AddScoped<IProductKeyManager, ProductKeyManager>();
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<Sms.Infobip.SmsSettings>(Configuration.GetSection("InfobipSettings"));

            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IExtendedEmailSender, EmailSender>();
            services.AddTransient<ISmsSender, Sms.Infobip.SmsSender>();
            services.AddScoped<AppMessages>();

            services.AddScoped<ICaptureSessionService, CaptureSessionService>();
            services.AddScoped<IVerifySessionService, VerifySessionService>();
            services.AddScoped<IDbSeeder, DbSeeder>();

            services.Configure<BiometricOptions>(options => Configuration.GetSection(nameof(BiometricOptions)).Bind(options));
            services.AddSingleton<ClickOnceHelper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseDatabaseErrorPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                app.UseExceptionHandler(new ExceptionHandlerOptions
                {
                    ExceptionHandler = async context =>
                    {
                        context.Response.ContentType = "text/html";
                        var ex = context.Features.Get<IExceptionHandlerFeature>();
                        if (ex != null)
                        {
                            var err = $"<h1>Error: {ex.Error.Message}</h1>";
                            await context.Response.WriteAsync(err);
                        }
                    }
                });
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAnonymousId();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseFormHelper();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "MyAreas",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
                endpoints.MapHub<CaptureSessionHub>("/captureSessionHub");
                endpoints.MapHub<VerifySessionHub>("/verifySessionHub");
            });

            var seeder = serviceProvider.GetService<IDbSeeder>();
            seeder.InitializeUser();
            seeder.InitializeData();
            seeder.SeedFromFile("wwwroot/content/DbSeed.xlsx");
        }
    }
}

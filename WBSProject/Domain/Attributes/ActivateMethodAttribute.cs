﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ActivateMethodAttribute : Attribute
    {

    }
}

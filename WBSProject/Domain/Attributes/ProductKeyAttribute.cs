﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using WBSProject.Security;

namespace WBSProject.Attributes
{
    public class ProductKeyAttribute : ActionFilterAttribute
    {
        public const string ClaimType = "PKM-SM";

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var productKey = GetProductKey(context.HttpContext.User);
            var controllerDescription = context.ActionDescriptor as ControllerActionDescriptor;

            if (!controllerDescription.MethodInfo.IsDefined(typeof(ActivateMethodAttribute)))
            {
                if (productKey == null || productKey.Expired)
                {
                    context.Result = new RedirectResult($"/Home/Activate?returnUrl={context.HttpContext.Request.Path.Value}");
                }
            }
        }

        private ProductKey GetProductKey(ClaimsPrincipal principal)
        {
            var claim = principal.Claims.FirstOrDefault(c => c.Type == ClaimType);
            if (claim != null)
            {
                var productKey = JsonConvert.DeserializeObject<ProductKey>(claim.Value);
                return productKey;
            }
            return null;
        }
    }
}

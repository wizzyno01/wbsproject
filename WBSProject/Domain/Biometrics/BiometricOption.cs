﻿namespace WBSProject.Biometrics
{
    public class BiometricOption
    {
        public string Path { get; set; }
        public string Api { get; set; }
    }
}

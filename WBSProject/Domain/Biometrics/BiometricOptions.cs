﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Biometrics
{
    public class BiometricOptions
    {
        public string RequestBasePath { get; set; }
        public string ResponseBasePath { get; set; }
        public BiometricOption Capture { get; set; }
        public BiometricOption Verify { get; set; }
    }
}

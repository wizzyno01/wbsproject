﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using WBSProject.Data;
using WBSProject.Models.EF;

namespace WBSProject.Biometrics
{
    public static class BiometricExtensions
    {
        public static async Task<Guid> GetOrAddCaptureSession(this IPrincipal principal, ApplicationDbContext context)
        {
            var captureSession = await context.CaptureSessions.FirstOrDefaultAsync(s => s.State == SessionState.Open);

            if (captureSession != null)
                return captureSession.Id;

            var sessionId = Guid.NewGuid();

            captureSession = new CaptureSession
            {
                Id = sessionId,
                State = SessionState.Open,
                CreatedBy = principal.Identity.Name,
                CreatedOn = DateTime.Now,
                SessionData = new CaptureSessionData { Id = sessionId }
            };
            context.CaptureSessions.Add(captureSession);
            await context.SaveChangesAsync();

            return sessionId;
        }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WBSProject.Attributes;
using WBSProject.Models.Identity;
using WBSProject.Security;

namespace WBSProject
{
    public class ApplicationUserClaimsPrincipalFactory : UserClaimsPrincipalFactory<ApplicationUser>
    {
        private readonly IProductKeyManager productKeyManager;
        public ApplicationUserClaimsPrincipalFactory(UserManager<ApplicationUser> userManager,
            IOptions<IdentityOptions> optionsAccessor, IProductKeyManager productKeyManager)
            : base(userManager, optionsAccessor)
        {
            this.productKeyManager = productKeyManager;
        }

        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(ApplicationUser user)
        {
            var identity = await base.GenerateClaimsAsync(user);
            var roles = await UserManager.GetRolesAsync(user);

            identity.AddClaim(new Claim(Globals.ClaimTypes.DisplayName, user.FullName));
            identity.AddClaim(new Claim(Globals.ClaimTypes.UserId, user.Id));
            identity.AddClaim(new Claim(Globals.ClaimTypes.SuperUser, user.IsSuperUser.ToString().ToLower()));
            identity.AddClaim(new Claim(Globals.ClaimTypes.UserType, user.Type.ToString().ToLower()));

            foreach (var role in roles)
                identity.AddClaim(new Claim(ClaimTypes.Role, role));

            var productKey = productKeyManager.GetProductKey();

            if (!string.IsNullOrEmpty(productKey))
                identity.AddClaim(new Claim(ProductKeyAttribute.ClaimType, productKey));

            return identity;
        }
    }

}

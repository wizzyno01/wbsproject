﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Areas.WBS.Models;
using WBSProject.Models;
using WBSProject.Models.EF;

namespace WBSProject
{
    public class AppProfile : Profile
    {
        public AppProfile()
        {
            CreateMap<EditMessageTemplate, MessageTemplate>().ReverseMap();
            CreateMap<QuestionierViewModel, ApplicantInfo>().ReverseMap()
                .ForMember(x => x.RCNumber, opt => opt.MapFrom(src => src.RCNumber))
                .ForMember(x => x.BusinessStatus, opt => opt.MapFrom(src => src.BusinessStatus));
            CreateMap<RegisterApplicantPersonal, ApplicantInfo>().ReverseMap();
            CreateMap<RegisterApplicantBusiness, ApplicantInfo>().ReverseMap();
            CreateMap<RegisterApplicantContact, ApplicantInfo>().ReverseMap();
            CreateMap<RegisterApplicantGrant, GrantInfo>()
                .ForMember(x => x.Employees, opt => opt.Ignore())
                .ForMember(x => x.DLI, opt => opt.Ignore()).ReverseMap();
            CreateMap<ApplicantInfo, RegistrationSummary>();

            CreateMap<BiometricInfo, BiometricStatus>();
            CreateMap<ApplicantInfo, ApplicantStatus>()
                .ForMember(x => x.State, opt => opt.MapFrom(src => src.State.Name))
                .ForMember(x => x.LGA, opt => opt.MapFrom(src => src.LGA.Name))
                .ForMember(x => x.Ward, opt => opt.MapFrom(src => src.Ward.Name));
            CreateMap<GrantInfo, GrantStatus>()
                .ForMember(x => x.DLI, opt => opt.MapFrom(src => src.DLI.Name));
            CreateMap<BiometricInfo, ApplicationStatus>()
                .ForMember(x => x.Biometric, opt => opt.MapFrom(src => src));
            CreateMap<ApplicantInfo, ApplicationStatus>()
                .ForMember(x => x.Applicant, opt => opt.MapFrom(src => src));
            CreateMap<GrantInfo, ApplicationStatus>()
                .ForMember(x => x.Grant, opt => opt.MapFrom(src => src));
        }
    }
}

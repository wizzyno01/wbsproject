﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject;
using WBSProject.Data;
using WBSProject.Helpers;
using WBSProject.Models;
using WBSProject.Models.EF;

namespace WBSProject.Hubs
{
    public class CaptureSessionHub : Hub
    {
        private static readonly ConcurrentDictionary<string, UserHubModel> Users =
            new ConcurrentDictionary<string, UserHubModel>(StringComparer.InvariantCultureIgnoreCase);

        private readonly ApplicationDbContext _context;

        public CaptureSessionHub(ApplicationDbContext context)
        {
            _context = context;
        }

        public override Task OnConnectedAsync()
        {
            string userName = Context.User.Identity.Name;
            string connectionId = Context.ConnectionId;

            var user = Users.GetOrAdd(userName, _ => new UserHubModel
            {
                UserName = userName,
                ConnectionIds = new HashSet<string>()
            });

            lock (user.ConnectionIds)
            {
                user.ConnectionIds.Add(connectionId);
            }

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            string userName = Context.User.Identity.Name;
            string connectionId = Context.ConnectionId;

            Users.TryGetValue(userName, out UserHubModel user);

            if (user != null)
            {
                lock (user.ConnectionIds)
                {
                    user.ConnectionIds.RemoveWhere(cid => cid.Equals(connectionId));
                    if (!user.ConnectionIds.Any())
                    {
                        Users.TryRemove(userName, out UserHubModel removedUser);
                    }
                }
            }

            return base.OnDisconnectedAsync(exception);
        }

        public async Task<CaptureSession> GetCaptureSession(Guid session)
        {
            var captureSession = await _context.CaptureSessions.SingleOrDefaultAsync(cs => cs.Id == session);
            return captureSession;
        }

        public async Task NotifyNewCapture(IHubContext<CaptureSessionHub> hubContext, Guid session)
        {
            var captureSession = await GetCaptureSession(session);
            var userName = captureSession.CreatedBy;

            if (Users.TryGetValue(userName, out UserHubModel receiver))
            {
                var mime = "image/jpeg";
                var model = new CaptureSessionViewModel
                {
                    Photograph = captureSession.SessionData.Photograph?.ToWebImage(mime),
                    RightThumb = captureSession.SessionData.RightThumbImage?.ToWebImage(mime),
                    RightIndex = captureSession.SessionData.RightIndexImage?.ToWebImage(mime),
                    LeftThumb = captureSession.SessionData.LeftThumbImage?.ToWebImage(mime),
                    LeftIndex = captureSession.SessionData.LeftIndexImage?.ToWebImage(mime),
                    LeftIndexMinutia = captureSession.SessionData.LeftThumbMinutia.ToBase64String(),
                    LeftThumbMinutia = captureSession.SessionData.LeftIndexMinutia.ToBase64String(),
                    RightIndexMinutia = captureSession.SessionData.RightThumbMinutia.ToBase64String(),
                    RightThumbMinutia = captureSession.SessionData.RightIndexMinutia.ToBase64String()
                };
                receiver.ConnectionIds.ToList().ForEach(async id =>
                {
                    await hubContext.Clients.Client(id).SendAsync("NotifyNewCapture", model);
                });
            }
        }

        public async Task NotifyNewCaptureDocs(IHubContext<CaptureSessionHub> hubContext, Guid session)
        {
            var captureSession = await GetCaptureSession(session);
            var userName = captureSession.CreatedBy;

            if (Users.TryGetValue(userName, out UserHubModel receiver))
            {
                var model = new CaptureSessionViewModel
                {
                    Documents = captureSession.SessionData.Documents?.ToWebPdf()
                };
                receiver.ConnectionIds.ToList().ForEach(async id =>
                {
                    await hubContext.Clients.Client(id).SendAsync("NotifyNewCaptureDocs", model);
                });
            }
        }
    }
}
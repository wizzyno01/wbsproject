﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WBSProject.Data;
using WBSProject.Helpers;
using WBSProject.Models;
using WBSProject.Models.EF;

namespace WBSProject.Hubs
{
    public class VerifySessionHub : Hub
    {
        private static readonly ConcurrentDictionary<string, UserHubModel> Users =
            new ConcurrentDictionary<string, UserHubModel>(StringComparer.InvariantCultureIgnoreCase);

        private readonly ApplicationDbContext _context;

        public VerifySessionHub(ApplicationDbContext context)
        {
            _context = context;
        }

        public override Task OnConnectedAsync()
        {
            string userName = Context.User.Identity.Name;
            string connectionId = Context.ConnectionId;

            var user = Users.GetOrAdd(userName, _ => new UserHubModel
            {
                UserName = userName,
                ConnectionIds = new HashSet<string>()
            });

            lock (user.ConnectionIds)
            {
                user.ConnectionIds.Add(connectionId);
            }

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            string userName = Context.User.Identity.Name;
            string connectionId = Context.ConnectionId;

            Users.TryGetValue(userName, out UserHubModel user);

            if (user != null)
            {
                lock (user.ConnectionIds)
                {
                    user.ConnectionIds.RemoveWhere(cid => cid.Equals(connectionId));
                    if (!user.ConnectionIds.Any())
                    {
                        Users.TryRemove(userName, out UserHubModel removedUser);
                    }
                }
            }

            return base.OnDisconnectedAsync(exception);
        }

        public async Task<CaptureSession> GetVerifySession(Guid session)
        {
            var verifySession = await _context.CaptureSessions.SingleOrDefaultAsync(cs => cs.Id == session);
            return verifySession;
        }

        public async Task NotifyNewVerify(IHubContext<VerifySessionHub> hubContext, Guid session)
        {
            var verifySession = await GetVerifySession(session);
            var userName = verifySession.CreatedBy;

            if (Users.TryGetValue(userName, out UserHubModel receiver))
            {
                var mime = "image/jpeg";
                var model = new VerifySessionViewModel
                {
                    Image = verifySession.SessionData.LeftIndexImage?.ToWebImage(mime),
                    Minutia = Convert.ToBase64String(verifySession.SessionData.LeftIndexMinutia)
                };
                receiver.ConnectionIds.ToList().ForEach(async id =>
                {
                    await hubContext.Clients.Client(id).SendAsync("NotifyNewVerify", model);
                });
            }
        }

        public void StatusUpdate(IHubContext<VerifySessionHub> hubContext, JobModel model)
        {
            if (Users.TryGetValue(model.Username, out UserHubModel receiver))
            {
                receiver.ConnectionIds.ToList().ForEach(async id =>
                {
                    await hubContext.Clients.Client(id).SendAsync("StatusUpdate", model);
                });
            }
        }

    }
}
﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Linq;
using System.Threading;
using WBSProject.Data;
using WBSProject.Models;

namespace WBSProject.Hubs
{
    public interface IVerifySessionService
    {
        void ScanForApplicant(Guid id);
        IHubContext<VerifySessionHub> Context { get; }
    }

    public class VerifySessionService : IVerifySessionService
    {
        private readonly ApplicationDbContext db;
        public VerifySessionService(ApplicationDbContext db, IHubContext<VerifySessionHub> context)
        {
            this.db = db;
            Context = context;
        }

        public void ScanForApplicant(Guid id)
        {
            var verifySession = db.CaptureSessions.SingleOrDefault(cs => cs.Id == id);
            var userName = verifySession.CreatedBy;
            var hub = new VerifySessionHub(db);

            var applicants = db.BiometricInfos.Select(p => new
            {
                p.Id,
                p.ApplicantId,
                p.FullName,
                p.LeftThumbTemplate,
                p.LeftIndexTemplate,
                p.RightThumbTemplate,
                p.RightIndexTemplate
            }).ToList();

            var totalPatient = applicants.Count;
            //var engine = new AfisEngine();
            //var unknown = new Person();
            //var index = 1;

            //unknown.Fingerprints.Add(new Fingerprint
            //{
            //    Template = verifySession.SessionData.Minutia
            //});

            foreach (var applicant in applicants)
            {
                var model = new JobModel
                {
                    // Completed = index == totalPatient,
                    Message = applicant.FullName,
                    //Processed = index,
                    Total = totalPatient,
                    Username = userName,
                    Score = 0
                };
                //var person = new Person();

                //person.Fingerprints.Add(new Fingerprint { Template = patient.LeftThumbTemplate });
                //person.Fingerprints.Add(new Fingerprint { Template = patient.LeftIndexTemplate });
                //person.Fingerprints.Add(new Fingerprint { Template = patient.RightThumbTemplate });
                //person.Fingerprints.Add(new Fingerprint { Template = patient.RightIndexTemplate });

                //var score = engine.Verify(unknown, person);

                //if (score > engine.Threshold)
                //{
                //    // score greater zero should be added to list with url
                //    model.Score = score;
                //}
                hub.StatusUpdate(Context, model);
                Thread.Sleep(1);

                //index++;
            }
        }

        public IHubContext<VerifySessionHub> Context { get; }
    }
}


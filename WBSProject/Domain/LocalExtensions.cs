﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace WBSProject
{
    public static class LocalExtensions
    {
        public static string ToBase64String(this byte[] bytes)
        {
            if (bytes == null)
                return null;
            return Convert.ToBase64String(bytes);
        }

        public static string StringCast(this object value)
        {
            if (value == null)
                return null;
            return value.ToString().Trim();
        }

        public static bool IsGuest(this ClaimsPrincipal principal)
        {
            if (principal == null || principal.HasClaim(ClaimTypes.Role, "Guest"))
                return true;
            return false;
        }

        public static string FullNames(this ClaimsPrincipal principal)
        {
            var displayNameClaim = principal.Claims.FirstOrDefault(c => c.Type == Globals.ClaimTypes.DisplayName);
            return displayNameClaim?.Value ?? principal.Identity.Name;
        }

        //const string IPV4_NETMASK = "255.255.255.0";
        //const string IPV6_NETMASK = "ffff:ffff:ffff:0000:0000:0000:0000:0000";

        /// <summary>
        /// Removes the unique part of an <see cref="IPAddress" />.
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns><see cref="string" /></returns>
        public static string AnonymizeIP(this IPAddress ipAddress)
        {
            string ipAnonymizedString;
            if (ipAddress != null)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    var ipString = ipAddress.ToString();
                    string[] octets = ipString.Split('.');
                    octets[3] = "0";
                    ipAnonymizedString = string.Join(".", octets);
                }
                else if (ipAddress.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    var ipString = ipAddress.ToString();
                    string[] hextets = ipString.Split(':');
                    var hl = hextets.Length;
                    if (hl > 3) { for (var i = 3; i < hl; i++) { if (hextets[i].Length > 0) { hextets[i] = "0"; } } }
                    ipAnonymizedString = string.Join(":", hextets);
                }
                else { ipAnonymizedString = $"Not Valid - {ipAddress.ToString()}"; }
            }
            else { ipAnonymizedString = "Is Null"; }

            return ipAnonymizedString;
        }
    }
}

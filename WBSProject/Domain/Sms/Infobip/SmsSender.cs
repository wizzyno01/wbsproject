﻿using Infobip.Api.Client;
using Infobip.Api.Client.Api;
using Infobip.Api.Client.Model;
using Microsoft.Extensions.Options;
using PhoneNumbers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Sms.Infobip
{
    public class SmsSender : ISmsSender
    {
        private readonly SmsSettings _smsSettings;
        public SmsSender(IOptions<SmsSettings> smsSettings)
        {
            _smsSettings = smsSettings.Value;
        }

        public Task SendSmsAsync(string message, string senderId, params string[] destinations)
        {
            try
            {
                var util = PhoneNumberUtil.GetInstance();
                var phoneNumbers = destinations.Select(d => util.Parse(d, "NG")).ToList();

                var smsMessage = new SmsTextualMessage
                {
                    From = senderId,
                    Destinations = phoneNumbers.Select(p => new SmsDestination(to: util.Format(p, PhoneNumberFormat.E164))).ToList(),
                    //Destinations = destinations.Select(d => new SmsDestination(to: d)).ToList(),
                    Text = message
                };
                var smsRequest = new SmsAdvancedTextualRequest
                {
                    Messages = new List<SmsTextualMessage> { smsMessage }
                };

                try
                {
                    var sendSmsApi = new SendSmsApi(new Configuration
                    {
                        BasePath = _smsSettings.BasePath,
                        ApiKey = _smsSettings.ApiKey,
                        ApiKeyPrefix = "App"
                    });
                    var smsResponse = sendSmsApi.SendSmsMessage(smsRequest);

                    System.Diagnostics.Debug.WriteLine($"Status: {smsResponse.Messages.First().Status}");
                    return Task.CompletedTask;
                }
                catch (ApiException ex)
                {
                    throw new InvalidOperationException($"{ex.ErrorCode}: {ex.Message}");
                }
            }
            catch
            {
                throw new InvalidOperationException("Invalid destination phone numbers.");
            }
        }
    }
}

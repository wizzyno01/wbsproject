﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Sms.Infobip
{
    public class SmsSettings
    {
        public string BasePath { get; set; }
        public string ApiKey { get; set; }
    }
}

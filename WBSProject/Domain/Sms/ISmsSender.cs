﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Sms
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string message, string senderId, params string[] destinations);
    }
}

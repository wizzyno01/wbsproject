using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Data;

namespace WBSProject.Domain.SharedComponent
{
   
    public interface ISharedComponents
    {
        string CalculateAge(DateTime DOB);
        string StateName(Guid? stateId);
        string LGaName(Guid? lgaId);
        string WardName(Guid? wardId);
    }

    public class SharedComponents :ISharedComponents
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SharedComponents(ApplicationDbContext context, IWebHostEnvironment environment,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _environment = environment;
            _httpContextAccessor = httpContextAccessor;
        }

        public string StateName(Guid? stateId)
        {
            return _context.States.SingleOrDefault(x => x.Id == stateId).Name;
        }

        public string LGaName(Guid? lgaId)
        {
            return _context.Lgas.SingleOrDefault(x => x.Id == lgaId).Name;
        }

        public string WardName(Guid? wardId)
        {
            return _context.Wards.SingleOrDefault(x => x.Id == wardId).Name;
        }


        public string CalculateAge(DateTime DOB)
        {
            var age = DateTime.Now.Year - DOB.Year;

            if (DateTime.Now.DayOfYear < DOB.DayOfYear)
            {
                age--;
                //age = age - 1;
            }

            var finalage = age.ToString();

            if (DOB == DateTime.MinValue)
            {
                finalage = "Unknown";
            }

            return finalage;
        }
    }
}

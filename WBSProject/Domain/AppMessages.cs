﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Data;
using WBSProject.Models.EF;

namespace WBSProject
{
    public class AppMessages : IEnumerable<MessageTemplate>
    {
        #region IEnumerable Members

        IEnumerator<MessageTemplate> IEnumerable<MessageTemplate>.GetEnumerator()
        {
            return _templates.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _templates.GetEnumerator();
        }

        #endregion

        private readonly ApplicationDbContext _context;
        private readonly List<MessageTemplate> _templates;

        public AppMessages(ApplicationDbContext context)
        {
            _context = context;
            _templates = RegisterTemplates();
        }

        public MessageTemplate Find(string category)
        {
            return this.FirstOrDefault(t => t.Category == category);
        }

        private List<MessageTemplate> RegisterTemplates()
        {
            var dbTemplates = _context.MessageTemplates.ToList();
            var registeredIds = dbTemplates.Select(t => t.Id).ToList();
            var templatesToRegister = Templates.Where(t => !registeredIds.Contains(t.Id)).ToList();

            if (templatesToRegister.Count > 0)
            {
                _context.MessageTemplates.AddRange(templatesToRegister);
                _context.SaveChanges();
                dbTemplates.AddRange(templatesToRegister);
            }
            return dbTemplates;
        }

        /// <summary>
        /// Manually add message templates that are to be used by the application here.
        /// </summary>
        private List<MessageTemplate> Templates
        {
            get
            {
                return new List<MessageTemplate>
                {
                    new MessageTemplate
                    {
                        Id = Guid.Parse("{F1045B4E-866D-4989-96C6-0942D9CCEC71}"),
                        Type = MessageType.Regular,
                        Category = Globals.MessageCategories.GuestDashboard.SubmitApplication,
                        Description = "SMS sent after application is submitted.",
                        Subject = null,
                        Content = "Your application was registered successfully."
                    },
                    new MessageTemplate
                    {
                        Id = Guid.Parse("{21674DCF-1606-46F2-A384-37C81209186F}"),
                        Type = MessageType.OTP,
                        Category = Globals.MessageCategories.VerificationCode,
                        Description = "One-Time-Password (OTP) message.",
                        Subject = null,
                        Content = "Your verification code is: {{pin}}"
                    },
                    new MessageTemplate
                    {
                        Id = Guid.Parse("{FF40B03B-E75A-426B-B149-2B1764397DC6}"),
                        Type = MessageType.HtmlEmail,
                        Category = Globals.MessageCategories.Security.EmailConfirmation,
                        Description = "Email confirmation message.",
                        Subject = "Confirm you email",
                        Content = "Please confirm your account by"
                    }
                };
            }
        }
    }
}

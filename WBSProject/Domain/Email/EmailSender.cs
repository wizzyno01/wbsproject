﻿using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace WBSProject.Email
{
    public interface IExtendedEmailSender : IEmailSender
    {
        Task SendEmailAsync(string subject, string textMessage);
        Task SendEmailAsync(string email, string subject, string textMessage, string htmlMessage);
    }

    public class EmailSender : IExtendedEmailSender
    {
        private readonly EmailSettings _emailSettings;
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EmailSender(IOptions<EmailSettings> emailSettings, IWebHostEnvironment environment,
            IHttpContextAccessor httpContextAccessor)
        {
            _emailSettings = emailSettings.Value;
            _environment = environment;
            _httpContextAccessor = httpContextAccessor;
        }

        public Task SendEmailAsync(string subject, string textMessage)
        {
            var _context = _httpContextAccessor.HttpContext;
            var _host = _context.Request.Host.ToString();
            var _uaString = _context.Request.Headers["User-Agent"].ToString();
            var _ipAnonymizedString = _context.Connection.RemoteIpAddress.AnonymizeIP();
            var _uid = _context.User.Identity.IsAuthenticated
                ? _context.User.FindFirst(ClaimTypes.NameIdentifier).Value
                : "Unknown";
            var _path = _context.Request.Path;
            var _queryString = _context.Request.QueryString;

            StringBuilder sb = new StringBuilder($"Host = {_host} \r\n");

            sb.Append($"User Agent = {_uaString} \r\n");
            sb.Append($"Anonymized IP = {_ipAnonymizedString} \r\n");
            sb.Append($"UserId = {_uid} \r\n");
            sb.Append($"Path = {_path} \r\n");
            sb.Append($"QueryString = {_queryString} \r\n \r\n");
            sb.Append(textMessage);

            return SendEmailAsync(_emailSettings.AdminEmail, subject, sb.ToString(), null);
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            return SendEmailAsync(email, subject, null, htmlMessage);
        }

        public async Task SendEmailAsync(string email, string subject, string textMessage, string htmlMessage)
        {
            var builder = new BodyBuilder { TextBody = textMessage, HtmlBody = htmlMessage };
            var mimeMessage = new MimeMessage();
            var isDevelopment = _environment.IsDevelopment();

            mimeMessage.From.Add(new MailboxAddress(_emailSettings.SenderName, _emailSettings.SenderEmail));
            mimeMessage.To.Add(MailboxAddress.Parse(email));
            mimeMessage.Subject = subject;
            mimeMessage.Body = builder.ToMessageBody();

            try
            {
                using var client = new SmtpClient
                {
                    ServerCertificateValidationCallback = (s, c, h, e) => true
                };

                if (isDevelopment)
                    await client.ConnectAsync(_emailSettings.MailServer, _emailSettings.MailPort, _emailSettings.UseSsl);
                else
                    await client.ConnectAsync(_emailSettings.MailServer).ConfigureAwait(false);

                // Note: only needed if the SMTP server requires authentication
                await client.AuthenticateAsync(_emailSettings.SenderEmail, _emailSettings.SenderPassword);
                await client.SendAsync(mimeMessage);
                await client.DisconnectAsync(true);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }
    }
}

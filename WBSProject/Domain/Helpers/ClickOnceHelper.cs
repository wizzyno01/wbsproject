﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WBSProject.Biometrics;

namespace WBSProject.Helpers
{
    public class ClickOnceHelper
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly BiometricOptions biometricOptions;
        public ClickOnceHelper(IHttpContextAccessor contextAccessor, IOptions<BiometricOptions> options)
        {
            _contextAccessor = contextAccessor;
            biometricOptions = options.Value;
        }

        private string GetBaseUrl(HttpRequest request)
        {
            var url = new Uri(request.GetDisplayUrl());

            if (url == null)
                return string.Empty;
            else
                return url.Scheme + "://" + url.Authority + request.PathBase.ToUriComponent() + "/";
        }

        public string PrepareCaptureAppUrl(Guid session, int appType)
        {
#if DEBUG
            var baseUrl = biometricOptions.RequestBasePath;
#else
            var baseUrl = GetBaseUrl(_contextAccessor.HttpContext.Request);
#endif
            var applicationPath = biometricOptions.Capture.Path;
            var apiPath = biometricOptions.Capture.Api;

#if DEBUG
            var base64Url = Convert.ToBase64String(Encoding.Default.GetBytes(biometricOptions.ResponseBasePath));
#else
            var base64Url = Convert.ToBase64String(Encoding.Default.GetBytes(baseUrl));
#endif
            var base64ApiPath = Convert.ToBase64String(Encoding.Default.GetBytes(apiPath));
            var url = $"{baseUrl}{applicationPath}?type={appType}&session={session}&url={base64Url}&resource={base64ApiPath}";

            return url;
        }

        public string PrepareVerifyAppUrl(Guid session)
        {
#if DEBUG
            var baseUrl = biometricOptions.RequestBasePath;
#else
            var baseUrl = GetBaseUrl(_contextAccessor.HttpContext.Request);
#endif
            var applicationPath = biometricOptions.Verify.Path;
            var apiPath = biometricOptions.Verify.Api;

#if DEBUG
            var base64Url = Convert.ToBase64String(Encoding.Default.GetBytes(biometricOptions.ResponseBasePath));
#else
            var base64Url = Convert.ToBase64String(Encoding.Default.GetBytes(baseUrl));
#endif
            var base64ApiPath = Convert.ToBase64String(Encoding.Default.GetBytes(apiPath));
            var url = $"{baseUrl}{applicationPath}?session={session}&url={base64Url}&resource={base64ApiPath}";

            return url;
        }

    }
}

﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Data;
using System.IO;
using DynamicReader;
using Microsoft.Extensions.FileProviders;

namespace WBSProject.Helpers
{
    public static class SelectListHelper
    {
        public static List<SelectListItem> Genders()
        {
            return "Male Female".Split(' ').Select(g => new SelectListItem
            {
                Text = g,
                Value = g
            }).ToList();
        }

        public static async Task<List<SelectListItem>> DLIs(this ApplicationDbContext dbContext, Guid? selected = null)
        {
            var dlis = await dbContext.DLIs.Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == selected
            }).ToListAsync();

            return dlis;
        }

        public static async Task<List<SelectListItem>> States(this ApplicationDbContext dbContext, Guid? selected = null)
        {
            var states = await dbContext.States.Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == selected
            }).ToListAsync();

            return states;
        }

        public static async Task<List<SelectListItem>> LGAs(this ApplicationDbContext dbContext, Guid? stateId, Guid? selected = null)
        {
            var lgas = await dbContext.Lgas.Where(lga => lga.StateId == stateId).Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == selected
            }).ToListAsync();

            return lgas;
        }

        public static async Task<List<SelectListItem>> Wards(this ApplicationDbContext dbContext, Guid? lgaId, Guid? selected = null)
        {
            var wards = await dbContext.Wards.Where(ward => ward.LgaId == lgaId).Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == selected
            }).ToListAsync();

            return wards;
        }

        public static List<SelectListItem> BusinessAges()
        {
            var businessAges = "Less than One Year, Less than Five Years, Five Years and Above";
            return businessAges.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> BusinessStatuses()
        {
            var businessStatuses = "Unregistered Registered";
            return businessStatuses.Split(' ').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> EmployeeNumbers()
        {
            var employeeNumbers = "Less than 10, 10 to 49, 50 and Above";
            return employeeNumbers.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> CoverageAreas()
        {
            var areaRanges = "Less than 100m radius, 100- 500 meters, 1 – 3 km, Over 3 km";
            return areaRanges.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> PersonAges()
        {
            var personAges = "Up to - 24 yrs, 24 -yrs to 45, Above 50 yrs";
            return personAges.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> RecordKeepings()
        {
            var recordKeepings = "Manual (entered in books), Digital (using any IT tools), Mental Note (no records at all)";
            return recordKeepings.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> HouseholdQuantities()
        {
            var householdQuantities = "1-3, 4-6, 7-10, Over 10";
            return householdQuantities.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> BusinessContributions()
        {
            var businessContributions = "10%, 25%, 50%, 100%";
            return businessContributions.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }
        
        
        public static List<SelectListItem> NigerianBanks()
        {

            var provider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            var fileInfo = provider.GetFileInfo("wwwroot/content/NigerianBanks.xlsx");

            if (!fileInfo.Exists)
                return null;

            var data = new List<SelectListItem>();

            using var reader = new ExcelReader(fileInfo.CreateReadStream());
            foreach (var worksheet in reader.Worksheets)
            {
                var key = worksheet.Name;

                switch (key)
                {
                    case "List Of Nigerian Banks":

                        var categories = worksheet.Read(cell => cell.Value != null,
                            (cell) => new SelectListItem
                            {
                                Text = cell.Value.ToString(),
                                Value = cell.Value.ToString()
                            }).ToList();
                        data.AddRange(categories);
                        break;
                }

            }

            return data;
        }

        public static List<SelectListItem> ICTTools()
        {
            var data = "Automated Accounting, Automated Invoicing, Ms Productivity Tool, Training, Website, E-Payment, Internet Services, Printers, Desktop, Laptops, POS, Tablets, Non";
            return data.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> AdvertsTypes()
        {
            var data = "Radio, Tv, Social Media, Word Of Mouth, Newspaper, Print Media, Event Marketing";
            return data.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> PaymentTypes()
        {
            var data = "Cash, E-Payment, POS, Barter";
            return data.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> RequireElectricity()
        {
            var data = "Neccessary, Very Neccessary, Not Neccessary";
            return data.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }

        public static List<SelectListItem> AlternatePowerSupply()
        {
            var data = "Solar, Generator, Rechargeable Lamp, Inverters, Batteries";
            return data.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }


        public static List<SelectListItem> ReasonsForMoreStaff()
        {
            var data = "Expansion, Increased Production";
            return data.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }


        public static List<SelectListItem> EffectsOfCOVID19()
        {
            var data = "Increased Demand/Sales, Decreased Demand/Sales, Shortage Of Raw Materials, Interruption In Supply Chain, Difficulty Accessing Funds";
            return data.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }


        public static List<SelectListItem> RegistrationCompanies()
        {
            var data = "CAC, SMEDAN, Cooperative ( provide name here )";
            return data.Split(',').Select(a => new SelectListItem
            {
                Text = a.Trim(),
                Value = a.Trim()
            }).ToList();
        }
        
    }
}

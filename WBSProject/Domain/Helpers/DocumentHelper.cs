﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WBSProject.Helpers
{
    public static class DocumentHelper
    {
        public static string ToWebPdf(this byte[] documents)
        {
            var mimeType = FileTypeHelper.GetContentType("Documents.pdf");
            return ToWebDocument(documents, mimeType);
        }

        public static string ToWebDocument(this byte[] documents)
        {
            return Convert.ToBase64String(documents);
        }

        public static string ToWebDocument(this byte[] documents, string mimeType)
        {
            var content = Convert.ToBase64String(documents);
            return $"data:{mimeType};base64,{content}";
        }

        public static byte[] FromWebDocument(this string documents)
        {
            var regex = new Regex(@"data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,");
            var content = regex.Replace(documents, "");

            return Convert.FromBase64String(content);
        }
    }
}

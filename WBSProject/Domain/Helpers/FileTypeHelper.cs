﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBSProject.Helpers
{
    /// <summary>
    /// Represents a Web mime-types helper class.
    /// </summary>
    public static class FileTypeHelper
    {
        /// <summary>
        /// Gets the mime-type from a url source filename.
        /// </summary>
        /// <param name="SourceFileName">Name of the source file.</param>
        /// <returns>System.String.</returns>
        public static string GetContentType(string SourceFileName)
        {
            var extension = Path.GetExtension(SourceFileName).ToLower();

            return extension switch
            {
                ".ai" => "application/postscript",
                ".aif" => "audio/x-aiff",
                ".aifc" => "audio/x-aiff",
                ".aiff" => "audio/x-aiff",
                ".asc" => "text/plain",
                ".au" => "audio/basic",
                ".avi" => "video/x-msvideo",
                ".bcpio" => "application/x-bcpio",
                ".bin" => "application/octet-stream",
                ".c" => "text/plain",
                ".cc" => "text/plain",
                ".ccad" => "application/clariscad",
                ".cdf" => "application/x-netcdf",
                ".class" => "application/octet-stream",
                ".cpio" => "application/x-cpio",
                ".cpp" => "text/plain",
                ".cpt" => "application/mac-compactpro",
                ".cs" => "text/plain",
                ".csh" => "application/x-csh",
                ".css" => "text/css",
                ".dcr" => "application/x-director",
                ".dir" => "application/x-director",
                ".dms" => "application/octet-stream",
                ".doc" => "application/msword",
                ".drw" => "application/drafting",
                ".dvi" => "application/x-dvi",
                ".dwg" => "application/acad",
                ".dxf" => "application/dxf",
                ".dxr" => "application/x-director",
                ".eps" => "application/postscript",
                ".etx" => "text/x-setext",
                ".exe" => "application/octet-stream",
                ".ez" => "application/andrew-inset",
                ".f" => "text/plain",
                ".f90" => "text/plain",
                ".fli" => "video/x-fli",
                ".flv" => "video/x-flv",
                ".gif" => "image/gif",
                ".gtar" => "application/x-gtar",
                ".gz" => "application/x-gzip",
                ".h" => "text/plain",
                ".hdf" => "application/x-hdf",
                ".hh" => "text/plain",
                ".hqx" => "application/mac-binhex40",
                ".htm" => "text/html",
                ".html" => "text/html",
                ".ice" => "x-conference/x-cooltalk",
                ".ief" => "image/ief",
                ".iges" => "model/iges",
                ".igs" => "model/iges",
                ".ips" => "application/x-ipscript",
                ".ipx" => "application/x-ipix",
                ".jpe" => "image/jpeg",
                ".jpeg" => "image/jpeg",
                ".jpg" => "image/jpeg",
                ".js" => "application/x-javascript",
                ".kar" => "audio/midi",
                ".latex" => "application/x-latex",
                ".lha" => "application/octet-stream",
                ".lsp" => "application/x-lisp",
                ".lzh" => "application/octet-stream",
                ".m" => "text/plain",
                ".man" => "application/x-troff-man",
                ".me" => "application/x-troff-me",
                ".mesh" => "model/mesh",
                ".mid" => "audio/midi",
                ".midi" => "audio/midi",
                ".mime" => "www/mime",
                ".mov" => "video/quicktime",
                ".movie" => "video/x-sgi-movie",
                ".mp2" => "audio/mpeg",
                ".mp3" => "audio/mpeg",
                ".mpe" => "video/mpeg",
                ".mpeg" => "video/mpeg",
                ".mpg" => "video/mpeg",
                ".mpga" => "audio/mpeg",
                ".ms" => "application/x-troff-ms",
                ".msh" => "model/mesh",
                ".nc" => "application/x-netcdf",
                ".oda" => "application/oda",
                ".pbm" => "image/x-portable-bitmap",
                ".pdb" => "chemical/x-pdb",
                ".pdf" => "application/pdf",
                ".pgm" => "image/x-portable-graymap",
                ".pgn" => "application/x-chess-pgn",
                ".png" => "image/png",
                ".pnm" => "image/x-portable-anymap",
                ".pot" => "application/mspowerpoint",
                ".ppm" => "image/x-portable-pixmap",
                ".pps" => "application/mspowerpoint",
                ".ppt" => "application/mspowerpoint",
                ".ppz" => "application/mspowerpoint",
                ".pre" => "application/x-freelance",
                ".prt" => "application/pro_eng",
                ".ps" => "application/postscript",
                ".qt" => "video/quicktime",
                ".ra" => "audio/x-realaudio",
                ".ram" => "audio/x-pn-realaudio",
                ".ras" => "image/cmu-raster",
                ".rgb" => "image/x-rgb",
                ".rm" => "audio/x-pn-realaudio",
                ".roff" => "application/x-troff",
                ".rpm" => "audio/x-pn-realaudio-plugin",
                ".rtf" => "text/rtf",
                ".rtx" => "text/richtext",
                ".scm" => "application/x-lotusscreencam",
                ".set" => "application/set",
                ".sgm" => "text/sgml",
                ".sgml" => "text/sgml",
                ".sh" => "application/x-sh",
                ".shar" => "application/x-shar",
                ".silo" => "model/mesh",
                ".sit" => "application/x-stuffit",
                ".skd" => "application/x-koan",
                ".skm" => "application/x-koan",
                ".skp" => "application/x-koan",
                ".skt" => "application/x-koan",
                ".smi" => "application/smil",
                ".smil" => "application/smil",
                ".snd" => "audio/basic",
                ".sol" => "application/solids",
                ".spl" => "application/x-futuresplash",
                ".src" => "application/x-wais-source",
                ".step" => "application/STEP",
                ".stl" => "application/SLA",
                ".stp" => "application/STEP",
                ".sv4cpio" => "application/x-sv4cpio",
                ".sv4crc" => "application/x-sv4crc",
                ".swf" => "application/x-shockwave-flash",
                ".t" => "application/x-troff",
                ".tar" => "application/x-tar",
                ".tcl" => "application/x-tcl",
                ".tex" => "application/x-tex",
                ".tif" => "image/tiff",
                ".tiff" => "image/tiff",
                ".tr" => "application/x-troff",
                ".tsi" => "audio/TSP-audio",
                ".tsp" => "application/dsptype",
                ".tsv" => "text/tab-separated-values",
                ".txt" => "text/plain",
                ".unv" => "application/i-deas",
                ".ustar" => "application/x-ustar",
                ".vcd" => "application/x-cdlink",
                ".vda" => "application/vda",
                ".vrml" => "model/vrml",
                ".wav" => "audio/x-wav",
                ".wrl" => "model/vrml",
                ".xbm" => "image/x-xbitmap",
                ".xlc" => "application/vnd.ms-excel",
                ".xll" => "application/vnd.ms-excel",
                ".xlm" => "application/vnd.ms-excel",
                ".xls" => "application/vnd.ms-excel",
                ".xlw" => "application/vnd.ms-excel",
                ".xml" => "text/xml",
                ".xpm" => "image/x-xpixmap",
                ".xwd" => "image/x-xwindowdump",
                ".xyz" => "chemical/x-pdb",
                ".zip" => "application/zip",
                _ => string.Format("application/{0}", extension),
            };
        }

        /// <summary>
        /// Determines whether the content-type is a valid image mime-type.
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        /// <returns><c>true</c> if it is valid; otherwise, <c>false</c>.</returns>
        public static bool IsValidImageContentType(string contentType)
        {
            switch (contentType)
            {
                // content types supported
                case "image/bmp":
                case "image/gif":
                case "image/jpe":
                case "image/jpeg":
                case "image/jpg":
                case "image/png":
                case "image/tif":
                case "image/tiff":
                    return true;

                // content types not yet supported
                case ".pnm":
                case ".ppm":
                case ".ras":
                case ".rgb":
                case ".pbm":
                case ".pgm":
                case ".ief":
                case ".xbm":
                case ".xpm":
                case ".xwd":
                    return false;
                default:
                    return false;
            }
        }

        public static ImageFormat GetImageFormat(string mimeType)
        {
            switch (mimeType)
            {
                // content types supported
                case "image/bmp":
                    return ImageFormat.Bmp;
                case "image/gif":
                    return ImageFormat.Gif;
                case "image/jpe":
                case "image/jpeg":
                case "image/jpg":
                    return ImageFormat.Jpeg;
                case "image/png":
                    return ImageFormat.Png;
                case "image/tif":
                case "image/tiff":
                    return ImageFormat.Tiff;
                case "image/wmf":
                case "image/x-wmf":
                    return ImageFormat.Wmf;

                default:
                    throw new NotSupportedException("The image format is not yet supported.");
            }
        }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using WBSProject.Models.EF;

namespace WBSProject.Security
{
    public class OS : AppAudit
    {
        /// <summary>
        /// Gets or sets the user instance ID.
        /// </summary>
        /// <value>The iid.</value>
        [MaxLength(50)]
        public string IID { get; set; }

        public Guid PTKId { get; set; }

        public PTK PTK { get; set; }
    }
}

﻿using System;

namespace WBSProject.Security
{
    public class ProductKeyUser
    {
        public Guid Id { get; set; }
        public string IID { get; set; }
    }
}

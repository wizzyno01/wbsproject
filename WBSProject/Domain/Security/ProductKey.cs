﻿using System;
using System.Collections.Generic;

namespace WBSProject.Security
{
    public class ProductKey
    {
        public Guid Id { get; set; }
        public string PID { get; set; }
        public int UsersAllowed { get; set; }
        public DateTime? ExpiresOn { get; set; }
        public bool Expired
        {
            get
            {
                if (ExpiresOn == null)
                    return false;
                return ExpiresOn < DateTime.Now;
            }
        }

        public List<ProductKeyUser> Users { get; set; }
    }
}

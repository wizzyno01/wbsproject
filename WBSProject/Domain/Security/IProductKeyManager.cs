﻿using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;
using WBSProject.Models.Identity;

namespace WBSProject.Security
{
    public interface IProductKeyManager
    {
        string GetProductKey();
        Task SignupKey(SignInManager<ApplicationUser> signInManager, ClaimsPrincipal principal, ProductKey productKey);
        string GenerateKey();
        bool Haskey();
        string CurrentIID { get; }
    }
}

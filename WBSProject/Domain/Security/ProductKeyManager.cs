﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ReturnTrue.AspNetCore.Identity.Anonymous;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WBSProject.Attributes;
using WBSProject.Data;
using WBSProject.Models.Identity;

namespace WBSProject.Security
{

    public class ProductKeyManager : IProductKeyManager
    {
        private readonly IHttpContextAccessor contextAccessor;
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> userManager;

        public string CurrentIID
        {
            get
            {
                IAnonymousIdFeature feature = contextAccessor.HttpContext.Features.Get<IAnonymousIdFeature>();
                return feature?.AnonymousId;
            }
        }

        public ProductKeyManager(IHttpContextAccessor contextAccessor, ApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            this.contextAccessor = contextAccessor;
            this.context = context;
            this.userManager = userManager;
        }

        public string GetProductKey()
        {
            IAnonymousIdFeature feature = contextAccessor.HttpContext.Features.Get<IAnonymousIdFeature>();
            string iid = feature?.AnonymousId;

            var productKey = context.PTKs.Include(pk => pk.Instances).Select(pk => new ProductKey
            {
                Id = pk.Id,
                PID = pk.PID,
                ExpiresOn = pk.ExpiresOn,
                UsersAllowed = pk.UsersAllowed,
                Users = pk.Instances.Select(i => new ProductKeyUser
                {
                    Id = i.Id,
                    IID = i.IID
                }).ToList()
            }).FirstOrDefault();

            if (productKey != null)
            {
                var user = productKey.Users.FirstOrDefault(u => u.IID == iid);

                if (user != null)
                {
                    var jsonProductKey = JsonConvert.SerializeObject(productKey);
                    return jsonProductKey;
                }
            }

            return null;
        }

        public async Task SignupKey(SignInManager<ApplicationUser> signInManager, ClaimsPrincipal principal, ProductKey productKey)
        {
            if (principal.Identity is ClaimsIdentity claimsIdentity)
            {
                var user = await userManager.GetUserAsync(principal);
                var jsonProductKey = JsonConvert.SerializeObject(productKey);
                var newClaim = new Claim(ProductKeyAttribute.ClaimType, jsonProductKey);

                claimsIdentity.AddClaim(newClaim);
                await signInManager.RefreshSignInAsync(user);
            }
        }

        public string GenerateKey()
        {
            var guid = Guid.NewGuid().ToString().ToUpper().Replace("-", "");
            var builder = new StringBuilder();
            string sep = null;

            for (int i = 0; i < 4; i++)
            {
                builder.Append(sep + guid.Substring(i * 4, 4));
                sep = "-";
            }
            return builder.ToString();
        }

        public bool Haskey()
        {
            return context.PTKs.Any();
        }
    }
}

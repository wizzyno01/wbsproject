﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WBSProject.Models.EF;

namespace WBSProject.Security
{
    public class PTK : AppAudit
    {
        [MaxLength(50)]
        public string PID { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the number of users allowed for the app.
        /// Zero means unlimited users.
        /// </summary>
        /// <value>The users allowed.</value>
        public int UsersAllowed { get; set; }
        public DateTime? ExpiresOn { get; set; }
        public bool Expired
        {
            get
            {
                if (ExpiresOn == null)
                    return false;
                return ExpiresOn < DateTime.Now;
            }
        }

        public virtual ICollection<OS> Instances { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject
{
    public static class Globals
    {
        public static class ClaimTypes
        {
            public static string UserType => "http://eds-micronet.com/claims/userType";
            public static string SuperUser => "http://eds-micronet.com/claims/superUser";
            public static string DisplayName => "http://eds-micronet.com/claims/displayName";
            public static string UserId => "http://eds-micronet.com/claims/userId";
        }

        public static class Areas
        {
            public const string WBS = "wbs";
            public const string SmartHealth = "smarthealth";
        }

        public static class Roles
        {
            public const string Guest = "Guest";
            public const string Admin = "Admin";
        }

        public static class MessageCategories
        {
            public const string VerificationCode = "WBS.VerificationCode";

            public static class Security
            {
                public const string EmailConfirmation = "WBS.Security.EmailConfirmation";
            }

            public static class GuestDashboard
            {
                public const string SubmitApplication = "WBS.GuestDashboard.SubmitApplication";
            }
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models
{
    public class UserHubModel
    {
        public string UserName { get; set; }
        public HashSet<string> ConnectionIds { get; set; }
    }

    public class CaptureSessionViewModel
    {
        public string Photograph { get; set; }
        public string RightThumb { get; set; }
        public string RightIndex { get; set; }
        public string LeftThumb { get; set; }
        public string LeftIndex { get; set; }
        public string RightThumbMinutia { get; set; }
        public string RightIndexMinutia { get; set; }
        public string LeftThumbMinutia { get; set; }
        public string LeftIndexMinutia { get; set; }
        public string Documents { get; set; }
    }

    public class VerifySessionViewModel
    {
        public string Image { get; set; }
        public string Minutia { get; set; }
    }


    public class JobModel
    {
        public string Username { get; set; }

        [JsonProperty("processed")]
        public int Processed { get; set; }
        [JsonProperty("total")]
        public int Total { get; set; }
        [JsonProperty("completed")]
        public bool Completed { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("score")]
        public float Score { get; set; }
    }

}

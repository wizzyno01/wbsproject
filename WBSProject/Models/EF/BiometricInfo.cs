﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public class BiometricInfo : AppAudit
    {
        public Guid ApplicantId { get; set; }
        public string FullName { get; set; }

        public byte[] Photo { get; set; }
        public byte[] RightThumbTemplate { get; set; }
        public byte[] RightThumbImage { get; set; }
        public byte[] RightIndexTemplate { get; set; }
        public byte[] RightIndexImage { get; set; }
        public byte[] LeftThumbTemplate { get; set; }
        public byte[] LeftThumbImage { get; set; }
        public byte[] LeftIndexTemplate { get; set; }
        public byte[] LeftIndexImage { get; set; }
        public byte[] Signature { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WBSProject.Models.EF
{
    public class Lga : AppAudit
    {
        [StringLength(50), MaxLength(50), Required]
        public string Name { get; set; }
        [StringLength(50), MaxLength(50)]
        public string District { get; set; }
        [Display(Name = "State")]
        public Guid StateId { get; set; }
        public virtual State State { get; set; }
        public virtual ICollection<Ward> Wards { get; set; }
    }
}

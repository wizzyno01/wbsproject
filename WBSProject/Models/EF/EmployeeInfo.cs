﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public class EmployeeInfo : AppAudit
    {
        [Required, MaxLength(128)]
        public string Names { get; set; }
        [Required, MaxLength(10)]
        public string Gender { get; set; }
        [Required, MaxLength(50)]
        public string Age { get; set; }

        public Guid GrantId { get; set; }
        
        public virtual GrantInfo Grant { get; set; }
    }
}

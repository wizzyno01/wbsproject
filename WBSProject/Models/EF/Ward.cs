﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WBSProject.Models.EF
{
    public class Ward : AppAudit
    {
        [StringLength(50), MaxLength(50), Required]
        public string Name { get; set; }
        public Guid LgaId { get; set; }
        public virtual Lga Lga { get; set; }
    }
}

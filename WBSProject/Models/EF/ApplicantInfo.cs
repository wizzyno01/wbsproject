﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public class ApplicantInfo : AppAudit
    {
        public string Username { get; set; }

        [Required, MaxLength(50)]
        public string Surname { get; set; }
        [Required, MaxLength(50), Display(Name = "First Name")]
        public string FirstName { get; set; }
        [MaxLength(50), Display(Name = "Other Name")]
        public string OtherName { get; set; }
        [Required, MaxLength(10)]
        public string Gender { get; set; }
        [Required, DataType(DataType.Date), Display(Name = "Birth Date")]
        public DateTime? BirthDate { get; set; }
        [Required, MaxLength(20), Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Required, MaxLength(50)]
        public string Email { get; set; }
        [Required, MaxLength(255)]
        public string Address { get; set; }

        /*
         * Lower properties not set to required to allow for partial
         * saving to the database (Profile first, the Business later).
         */
        [MaxLength(128), Display(Name = "Business Name")]
        public string BusinessName { get; set; }
        [MaxLength(255), Display(Name = "Nature of Business")]
        public string NatureOfBusiness { get; set; }
        [MaxLength(50), Display(Name = "Age of Business")]
        public string AgeOfBusiness { get; set; }
        /// <summary>
        /// Whether it is registered or not
        /// </summary>
        [MaxLength(20), Display(Name = "Business Status")]
        public string BusinessStatus { get; set; }
        [MaxLength(20), Display(Name = "RC Number")]
        public string RCNumber { get; set; }
        [MaxLength(50), Display(Name = "No. of Employees")]
        public string NumberOfEmployees { get; set; }
        [MaxLength(255), Display(Name = "Business Address")]
        public string BusinessAddress { get; set; }
        /// <summary>
        /// Makes information editable when it is general and only editable
        /// from back-end when it is official.
        /// </summary>
        [ScaffoldColumn(false)]
        public DataUsage Usage { get; set; }


        [Display(Name = "State")]
        public Guid? StateId { get; set; }
        [Display(Name = "LGA")]
        public Guid? LGAId { get; set; }
        [Display(Name = "Ward")]
        public Guid? WardId { get; set; }

        public virtual State State { get; set; }
        public virtual Lga LGA { get; set; }
        public virtual Ward Ward { get; set; }

        public string FullNames => $"{Surname}, {FirstName} {OtherName}";
        
         // These Data are not Mapped to the Database as columns
        [NotMapped]
        public string Age { get; set; }
        [NotMapped]
        public string StateName { get; set; }
        [NotMapped]
        public string LGAName { get; set; }
        [NotMapped]
        public string WardName { get; set; }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public class GeoLocation : AppAudit
    {
        public Guid ApplicantId { get; set; }
        public string BusinessName { get; set; }
        public string Description { get; set; }

        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string LandMark { get; set; }
        public byte[] FrontView { get; set; }
        public byte[] LeftSideView { get; set; }
        public byte[] RightSideView { get; set; }
        public byte[] BackView { get; set; }
    }
}

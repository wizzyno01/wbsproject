﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public class State : AppAudit
    {
        [StringLength(50), MaxLength(50), Required]
        public string Name { get; set; }
        public virtual ICollection<Lga> Lgas { get; set; }
    }
}

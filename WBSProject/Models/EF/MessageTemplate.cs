﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public enum MessageType
    {
        Regular,
        OTP,
        PlainEmail,
        HtmlEmail
    }

    public class MessageTemplate : AppAudit
    {
        /// <summary>
        /// An identifier that part of the application that
        /// requires the template can use to search it out.
        /// </summary>
        [Required, MaxLength(128)]
        public string Category { get; set; }
        /// <summary>
        /// A description for the category of the template.
        /// </summary>
        [Required, MaxLength(255)]
        public string Description { get; set; }
        public MessageType Type { get; set; }
        /// <summary>
        /// Subject if template is for email and 
        /// Sender ID if template is for SMS.
        /// </summary>
        [MaxLength(128)]
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public enum DocumentType
    {
        Other,
        Identification,
        UtilityBill,
        RefereeForms,
        Pdf
    }

    public class Document : AppAudit
    {
        public Guid ApplicantId { get; set; }
        public string FullNames { get; set; }
        public string BusinessName { get; set; }

        public DocumentType Type { get; set; }
        public string Notes { get; set; }
        public byte[] Content { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public enum SessionState
    {
        Open,
        Busy,
        Close
    }

    public class CaptureSession : AppAudit
    {
        public SessionState State { get; set; }
        [Required]
        public virtual CaptureSessionData SessionData { get; set; }
        public new DateTime CreatedOn { get { return base.CreatedOn; } set { base.CreatedOn = value; } }
        public new string CreatedBy { get { return base.CreatedBy; } set { base.CreatedBy = value; } }
        [NotMapped]
        public int DataType { get; set; }
    }

    public class CaptureSessionData
    {
        [Key, ForeignKey(nameof(Session))]
        public Guid Id { get; set; }

        public byte[] Photograph { get; set; }
        public byte[] RightThumbMinutia { get; set; }
        public byte[] RightThumbImage { get; set; }
        public byte[] RightIndexMinutia { get; set; }
        public byte[] RightIndexImage { get; set; }
        public byte[] LeftThumbMinutia { get; set; }
        public byte[] LeftThumbImage { get; set; }
        public byte[] LeftIndexMinutia { get; set; }
        public byte[] LeftIndexImage { get; set; }
        public byte[] Signature { get; set; }
        public byte[] Documents { get; set; }

        [NotMapped]
        public byte[] RightThumbTemplate { get; set; }
        [NotMapped]
        public byte[] RightIndexTemplate { get; set; }
        [NotMapped]
        public byte[] LeftThumbTemplate { get; set; }
        [NotMapped]
        public byte[] LeftIndexTemplate { get; set; }

        public virtual CaptureSession Session { get; set; }
    }
}

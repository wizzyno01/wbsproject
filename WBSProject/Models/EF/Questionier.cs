using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public class Questionier : AppAudit
    {
        public Guid ApplicantId { get; set; }

        public int? NumberOfStaff { get; set; }
        public bool ChangeInNumberOfStaff { get; set; }
        public DateTime? DateOfChangeInNumberOfStaff { get; set; }
        public bool IsStaffAdequate { get; set; }
        public string WhyMoreStaff { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal? StaffSalary { get; set; }
        public bool ChangeInStaffSalary { get; set; }
        public string ReasonForChangeInStaffSalary { get; set; }

        public DateTime? DateOfChangeInStaffSalary { get; set; }


        public bool ChangeInProductionVolume { get; set; }
        public string ReasonForChangeInProductionVolume { get; set; }

        public DateTime? DateOfChangeInProductionVolume { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal? MonthlyIncome { get; set; }
        public bool ChangeInMonthlyIncome { get; set; }
        public string ReasonForChangeInMonthlyIncome { get; set; }
        public DateTime? DateofChangeInMonthlyIncome { get; set; }


        // step 1 starts
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string BVN { get; set; }
        public string NIN { get; set; }
        public string StateOfOrigin { get; set; }
        public string LGAOfOrigin { get; set; }
        // step ends

        public string ICTToolInUse { get; set; }
        public string ICTToolNeeded { get; set; }
        public string CurrentMeansOfAdvertisement { get; set; }
        public bool IsCurrentMeansOfAdvertisementAdequate { get; set; }
        public string MethodOfReceivingPayment { get; set; }



        [Column(TypeName = "decimal(18,2)")]
        public decimal? AverageCostOfPHEDBills { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? AverageCostOfWaterBills { get; set; }
        public bool IsPayRent { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? CostOfMonthlyRent { get; set; }

        public string RequirePowerOrLight { get; set; }
        public string AlternatePower { get; set; }
        public DateTime? DateAlternatePowerWasinitialized { get; set; }


        public string RegistrationCompany { get; set; }
        public string COVID19EffectOnBusiness { get; set; }
        public bool HasAutomatedInvoicingSystem { get; set; }
        public bool HasWebsite { get; set; }
        public string WebsiteURL { get; set; }
        public string FacebookURL { get; set; }
        public string LinkedInURL { get; set; }
        public string TwitterURL { get; set; }

        public int Step { get; set; }

    }
}

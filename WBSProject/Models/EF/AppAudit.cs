﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public interface IAudit
    {
        void Audit(EntityState state, DateTime auditedOn, string currentUser);
    }

    public abstract class AppAudit : IAudit
    {
        protected AppAudit() { Id = Guid.NewGuid(); }

        [Key]
        public Guid Id { get; set; }
        [Column, MaxLength(50), ScaffoldColumn(false)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "datetime2"), ScaffoldColumn(false)]
        protected DateTime CreatedOn { get; set; }
        [Column, MaxLength(50), ScaffoldColumn(false)]
        protected string ModifiedBy { get; set; }
        [Column(TypeName = "datetime2"), ScaffoldColumn(false)]
        protected DateTime? ModifiedOn { get; set; }
        [Column, MaxLength(50), ScaffoldColumn(false), Timestamp]
        protected virtual byte[] RowVersion { get; set; }
        [Column, ScaffoldColumn(false)]
        protected int Revision { get; set; }

        void IAudit.Audit(EntityState state, DateTime auditedOn, string currentUser)
        {
            ModifiedBy = currentUser;
            ModifiedOn = auditedOn;

            if (state == EntityState.Added)
            {
                CreatedBy = currentUser;
                CreatedOn = auditedOn;
            }
        }
    }
}

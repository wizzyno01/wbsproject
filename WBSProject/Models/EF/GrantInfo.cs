﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.EF
{
    public class GrantInfo : AppAudit
    {
        public Guid ApplicantId { get; set; }
        public string FullNames { get; set; }
        public string BusinessName { get; set; }

        /// <summary>
        /// How much goods does your business have in stock.
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal? NetWorthOfGoods { get; set; }
        /// <summary>
        /// How much cash does your business have in the bank.
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal? CashInBank { get; set; }

        /// <summary>
        /// How far from location do customers come ..?
        /// </summary>
        public string DistanceFromCustomers { get; set; }
        public bool HaveWater { get; set; }
        public bool HaveElectricity { get; set; }
        public bool HaveRoad { get; set; }
        public bool HaveMedicalCare { get; set; }
        public bool HaveNetwork { get; set; }

        [MaxLength(10)]
        public string OwnersGender { get; set; }
        /// <summary>
        /// Age of promoter (Should give considerations to older persons who are more vulnerable)
        /// </summary>
        [MaxLength(50)]
        public string PromotersAge { get; set; }
        public virtual ICollection<EmployeeInfo> Employees { get; set; }

        [MaxLength(255)]
        public string DescribeComputerSkills { get; set; }
        public bool OwnSmartPhone { get; set; }
        public bool HaveFacebook { get; set; }
        public bool HaveTwitter { get; set; }
        public bool HaveWhatsApp { get; set; }
        public bool HaveInternet { get; set; }
        public bool HaveRecordKeeping { get; set; }
        /// <summary>
        /// If you have record keeping, describe eg. Manual, Digital, Mental
        /// </summary>
        [MaxLength(50)]
        public string DescribeRecordKeeping { get; set; }

        /// <summary>
        /// How many people living in the household
        /// </summary>
        [MaxLength(50)]
        public string NoOfPersonsInHousehold { get; set; }
        /// <summary>
        /// What is the contribution of the business to livelihood
        /// </summary>
        [MaxLength(50)]
        public string ContributionToLivelihood { get; set; }

        public Guid? DLIId { get; set; }

        public virtual DLI DLI { get; set; }

        public bool CanSubmit => DLIId != null && NetWorthOfGoods != null && CashInBank != null && !string.IsNullOrEmpty(OwnersGender);
    }
}

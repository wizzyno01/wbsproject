﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.Identity
{
    public enum ApplicationUserType
    {
        Guest,
        User,
        Admin,
        Audit
    }

    public class ApplicationUser : IdentityUser
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public ApplicationUserType Type { get; set; }

        public bool IsSuperUser { get; set; }

        public string FullName => $"{FirstName} {LastName}";
    }
}

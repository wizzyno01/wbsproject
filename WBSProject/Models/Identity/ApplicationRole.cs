﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WBSProject.Models.Identity
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
        { }
        public ApplicationRole(string name, string description)
        {
            Name = name;
            Description = description;
        }
        public string Description { get; set; }
    }
}
